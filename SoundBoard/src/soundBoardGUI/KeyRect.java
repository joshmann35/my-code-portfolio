package soundBoardGUI;

import java.awt.Rectangle;

public class KeyRect extends Rectangle{
	private String sound;
	public KeyRect(int x, int y, int w, int h, String s)
	{
		super(x,y,w,h);
		this.sound=s;
	}
	public void setSound(String s)
	{
		this.sound=s;
	}
	public String getSound()
	{
		return sound;
	}
}
