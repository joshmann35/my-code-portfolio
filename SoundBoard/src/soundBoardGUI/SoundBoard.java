package soundBoardGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.Timer;

import soundBoardSoundHandler.MediaPlayer;

public class SoundBoard implements KeyListener {
	JFrame frame;
	SoundBoardView panel;
	private KeyRect[] keys = new KeyRect[26];
	MediaPlayer mp = new MediaPlayer();
	Timer cycle = new Timer(10, new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent arg0) {
			panel.repaint();
		}
		
	});
	public SoundBoard()
	{
		instantiateKeys();
		frame = new JFrame("Joe's Sound Board v0.1");
		frame.setSize(750,325);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addKeyListener(this);
		frame.setFocusable(true);
		frame.requestFocusInWindow();
		frame.setVisible(true);
		panel = new SoundBoardView(keys);
		frame.getContentPane().add(panel);
		panel.repaint();
		cycle.start();
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
		mp.stop();
		switch(e.getKeyChar())
		{
		case 'q': new Thread(new MediaPlayer(keys[0].getSound()+".wav")).start(); break;
		case 'w': new Thread(new MediaPlayer(keys[1].getSound()+".wav")).start();  break;
		case 'e': new Thread(new MediaPlayer(keys[2].getSound()+".wav")).start();  break;
		case 'r': new Thread(new MediaPlayer(keys[3].getSound()+".wav")).start();  break;
		case 't': new Thread(new MediaPlayer(keys[4].getSound()+".wav")).start();  break;
		case 'y': new Thread(new MediaPlayer(keys[5].getSound()+".wav")).start();  break;
		case 'u': new Thread(new MediaPlayer(keys[6].getSound()+".wav")).start();  break;
		case 'i': new Thread(new MediaPlayer(keys[7].getSound()+".wav")).start();  break;
		case 'o': new Thread(new MediaPlayer(keys[8].getSound()+".wav")).start();  break;
		case 'p': new Thread(new MediaPlayer(keys[9].getSound()+".wav")).start();  break;
		case 'a': new Thread(new MediaPlayer(keys[10].getSound()+".wav")).start();  break;
		case 's': new Thread(new MediaPlayer(keys[11].getSound()+".wav")).start(); break;
		case 'd': new Thread(new MediaPlayer(keys[12].getSound()+".wav")).start(); break;
		case 'f': new Thread(new MediaPlayer(keys[13].getSound()+".wav")).start(); break;
		case 'g': new Thread(new MediaPlayer(keys[14].getSound()+".wav")).start(); break;
		case 'h': new Thread(new MediaPlayer(keys[15].getSound()+".wav")).start();  break;
		case 'j': new Thread(new MediaPlayer(keys[16].getSound()+".wav")).start();  break;
		case 'k': new Thread(new MediaPlayer(keys[17].getSound()+".wav")).start();  break;
		case 'l': new Thread(new MediaPlayer(keys[18].getSound()+".wav")).start();  break;
		case 'z': new Thread(new MediaPlayer(keys[19].getSound()+".wav")).start();  break;
		case 'x': new Thread(new MediaPlayer(keys[20].getSound()+".wav")).start();  break;
		case 'c': new Thread(new MediaPlayer(keys[21].getSound()+".wav")).start();  break;
		case 'v': new Thread(new MediaPlayer(keys[22].getSound()+".wav")).start();  break;
		case 'b': new Thread(new MediaPlayer(keys[23].getSound()+".wav")).start();  break;
		case 'n': new Thread(new MediaPlayer(keys[24].getSound()+".wav")).start();  break;
		case 'm': new Thread(new MediaPlayer(keys[25].getSound()+".wav")).start();  break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	public void instantiateKeys()
	{
		keys[0] = new KeyRect(74, 64 ,48, 54, "drum");
		keys[1] = new KeyRect(125, 64 ,48, 54, "crickets");
		keys[2] = new KeyRect(176, 64 ,48, 54, "double_kill");
		keys[3] = new KeyRect(227, 64 ,48, 54, "sc_charge");
		keys[4] = new KeyRect(278, 64 ,48, 54, "trololo");
		keys[5] = new KeyRect(329, 64 ,48, 54, "epic_sax");
		keys[6] = new KeyRect(380, 64 ,48, 54, "game_over");
		keys[7] = new KeyRect(421, 64 ,48, 54, "headshot");
		keys[8] = new KeyRect(482, 64 ,48, 54, "killing_spree");
		keys[9] = new KeyRect(533, 64 ,48, 54, "killtacular");
		keys[10] = new KeyRect(86, 121 ,48, 54, "air_horn");
		keys[11] = new KeyRect(137, 121 ,48, 54, "sh'dynasty");
		keys[12] = new KeyRect(188, 121 ,48, 54, "ding");
		keys[13] = new KeyRect(239, 121 ,48, 54, "slayer");
		keys[14] = new KeyRect(290, 121 ,48, 54, "suck_it");
		keys[15] = new KeyRect(341, 121 ,48, 54, "triple_kill");
		keys[16] = new KeyRect(392, 121 ,48, 54, "john_cena");
		keys[17] = new KeyRect(443, 121 ,48, 54, "untouchable");
		keys[18] = new KeyRect(494, 121 ,48, 54, "whip");
		keys[19] = new KeyRect(109, 179 ,48, 54, "you_cant_touch_this");
		keys[20] = new KeyRect(160, 179 ,48, 54, "woof");
		keys[21] = new KeyRect(211, 179 ,48, 54, "meow");
		keys[22] = new KeyRect(262, 179 ,48, 54, "quack");
		keys[23] = new KeyRect(313, 179 ,48, 54, "beckfast");
		keys[24] = new KeyRect(364, 179 ,48, 54, "clap");
		keys[25] = new KeyRect(415, 179 ,48, 54, "puk_puk");
	}
}
