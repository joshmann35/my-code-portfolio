package soundBoardGUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import soundBoardSoundHandler.MediaPlayer;

public class SoundBoardView extends JComponent{
	private KeyRect[] keys;
	private boolean[] isOn = new boolean[26];
	
	public SoundBoardView(KeyRect[] key)
	{
		keys = key;
		isOn[0]=false;
		isOn[1]=false;
		isOn[2]=false;
		isOn[3]=false;
		isOn[4]=false;
		isOn[5]=false;
		isOn[6]=false;
		isOn[7]=false;
		isOn[8]=false;
		isOn[9]=false;
		isOn[10]=false;
		isOn[11]=false;
		isOn[12]=false;
		isOn[13]=false;
		isOn[14]=false;
		isOn[15]=false;
		isOn[16]=false;
		isOn[17]=false;
		isOn[18]=false;
		isOn[19]=false;
		isOn[20]=false;
		isOn[21]=false;
		isOn[22]=false;
		isOn[23]=false;
		isOn[24]=false;
		isOn[25]=false;
		this.addMouseMotionListener(new MouseMotionListener(){

			@Override public void mouseDragged(MouseEvent arg0) {}

			@Override
			public void mouseMoved(MouseEvent e) {
				for(int i = 0; i < keys.length; i++)
				{
					if(keys[i].contains(e.getPoint()))
					{
						setToolTipText(keys[i].getSound());
						//System.out.println(keys[i].getSound());
						//System.out.println("Im Trying");
						isOn[i]=true;
					}
					else
					{
						//setToolTipText(null);
						isOn[i]=false;
					}
				}
			}
			
		});
		this.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent arg0) {
				for(int i = 0; i < isOn.length; i++)
				{
					if(isOn[i])
					{
						new Thread(new MediaPlayer(keys[i].getSound()+".wav")).start();
					}
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {}

			@Override
			public void mouseExited(MouseEvent arg0) {}

			@Override
			public void mousePressed(MouseEvent arg0) {}

			@Override
			public void mouseReleased(MouseEvent arg0) {}
			
		});
	}
	
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		Image bg = null;
		try {
			//File b = new File(System.getProperty("user.dir")+"background/qwerty.jpg");
			//System.out.println(b.getPath());
			bg = ImageIO.read(new File(System.getProperty("user.dir")+"/background/qwerty.jpg"));
		} catch (IOException e) {}
		g2.drawImage(bg, 0, 0, null);
		Color myColor = new Color((int)(Math.random()*256), (int)(Math.random()*256), (int)(Math.random()*256), 125);
		g2.setColor(myColor);
		for(int i = 0; i < isOn.length; i++)
		{
			if(isOn[i])
			{
				g2.fill(keys[i].getBounds());
			}
		}
		//g2.fillRect(10, 10, 20, 20);
	}
	public void setIsOn(int index, boolean state)
	{
		isOn[index] = state;
	}
}
