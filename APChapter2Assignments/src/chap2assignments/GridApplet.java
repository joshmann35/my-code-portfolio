
package chap2assignments;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

@SuppressWarnings("serial")
public class GridApplet extends Applet
{
	public void paint(Graphics page){
		Random g = new Random();
		int mult = 200;
		boolean color = true;
		int res = 1920/mult;
		while(true)
		{
			if(color)
			{
				switch(g.nextInt(3)) 
				{
				case 0: page.setColor(Color.blue);break;
				case 1: page.setColor(Color.green);break;
				case 2: page.setColor(Color.red);break;
				}
			}
			else
			{
				switch(g.nextInt(2))
				{
				case 0: page.setColor(Color.black);break;
				case 1: page.setColor(Color.WHITE);
				}
			}
			
			int x = g.nextInt(res)*mult;
			int y = g.nextInt(res)*mult;
			page.fillRect(x, y, mult, mult);
		}
	}
}
