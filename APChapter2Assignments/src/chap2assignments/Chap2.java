
package chap2assignments;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Chap2
{
	//CHAPTER 2 ASSIGNMENTS
	public static void main(String[] args)
	{
		
		// 2.7
		Scanner in = new Scanner(System.in);
		System.out.print("Enter the amount of seconds: ");
		int sec = in.nextInt();
		System.out.println(sec+" seconds is "+((sec/60)/60)%60+" hours and "+(sec/60)%60+" minutes and "+(sec%60)+" seconds");
		
		// 2.8
		System.out.print("Input x1: ");
		int x1 = in.nextInt();
		System.out.print("Input y1: ");
		int y1= in.nextInt();
		System.out.print("Input x2: ");
		int x2 = in.nextInt();
		System.out.print("Input y2: ");
		int y2 = in.nextInt();
		double distance=Math.sqrt(Math.abs(Math.pow(x2-x1, 2)-Math.pow(y2+y1, 2)));
		System.out.println("The distance inbetween the two points is "+distance);
		
		// 2.12
		DecimalFormat df = new DecimalFormat();
		df.applyLocalizedPattern("0.00");
		System.out.print("Enter amount of quarters: ");
		int quart = in.nextInt();
		System.out.print("Enter amount of dimes: ");
		int dime = in.nextInt();
		System.out.print("Enter amount of nickels: ");
		int nickel = in.nextInt();
		System.out.print("Enter amount of pennies: ");
		int penny = in.nextInt();
		double money = 0;
		money += quart*.25;
		money += dime*.1;
		money += nickel*.05;
		money += penny*.01;
		System.out.println("Total money is "+df.format(money)+" dollars");
	}
}

// REST OF QUESTIONS

/*
 * M/C
2.1) C
2.2) E
2.3) D
2.4) B
2.5) A
2.6) E
2.7) E
2.8) C
2.9) D
2.10) B
T/F
2.1) T
2.2) F
2.3) T
2.4) T
2.5) T
2.6) F
2.7) T
2.8) T
Short Answer
2.1)
	You call the method println in the out class int the System package. Doing so then outputs the string, "I gotta be me!", to the console.
2.2)
	OUTPUT:
	Here we go!12345
	Test this if you are not sure.Another.
	All done.
	:END
	The reason it doesnot go to the next line is because when you add the ln at the end of print, you are telling it to move to the next line after printing the string on the current line.
2.3)
	The line of code cannot continue onto the next line.
	System.out.println("To be or not to be, that is the question.");
2.4)
	OUTPUT:
	50 plus 25 is 5025
	:END
	This is because to do a math statement in a output, you would need to encapsulate the 50+25 is parenthesis like so, sysout("50 plus 25 is "+(50+25));
2.5)
	OUTPUT:
	He thrusts his fists
		against the post
	and still insists
		he sees the "ghost"
	:END
	They use escape functions that go to next line and tab
2.6)
	a) 5
	b) 5.0
	c) 3
	d) 3.0
	e) 3.4
	f) 1.330203443
	g) 0
	h) 0.625
	i) 0.625
	j) 0.0
	k) 3
	l) 3.0
	m) 0.0
	n) 2
	o) 6
	p) 0
	q) 0
2.7)
	a) 1 2 3
	b) 1 2 3
	C) 3 1 2
	d) 3 1 2
	e) 1 2 3
	f) 1 2 3
	g) 1 2 3
	h) 2 1 3
	i) 2 1 3
	j) 3 1 2
	k) 2 1 3 4
	l) 3 1 2 4
	m) 1 2 4 3
	n) 1 3 2 4
2.8) 
	enum DaysOfWeek{Sunday, Monday, Tuesday, Wednesday, Thursday, Friday};
	DaysOfWeek sunday = DaysOfWeek.Sunday;
2.9)
	OUTPUT:
	Quest for the Holy Grail quest for the zoly grail
2.10)
	double num3 = Math.sqrt(num1+num2);
2.11)
	System.out.println(Math.abs(total));
2.12) 
	a) 0 to 19
	b) 1 to 8
	c) 10 to 54
	d) -50 to 49
2.13)
	Random rand = new Random();
	a) rand.nextInt(11);
	b) rand.nextInt(501);
	c) rand.nextInt(10)+1;
	d) rand.nextInt(500)+1;
	e) rand.nextInt(26)+25;
	f) rand.nextInt(26)-10;
2.14)
	a) Math.rand()*11;
	b) Math.rand()*501;
	c) Math.rand()*10+1;
	d) Math.rand()*500+1;
	e) Math.rand()*26+25;
	f) Math.rand()*26-10;
 */

