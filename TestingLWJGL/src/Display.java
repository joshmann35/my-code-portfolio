/*
 * The MIT License (MIT)
 *
 * Copyright � 2014, Heiko Brumme
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetFramebufferSize;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL11.glVertex3f;
import static org.lwjgl.opengl.GL11.glVertex4f;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.IntBuffer;
import java.util.LinkedList;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

/**
 * This class is a simple quick starting guide. This is mainly a java conversion
 * of the
 * <a href=http://www.glfw.org/docs/latest/quick.html>Getting started guide</a>
 * from the official GLFW3 homepage.
 *
 * @author Heiko Brumme
 */
public class Display {

    /**
     * This error callback will simply print the error to
     * <code>System.err</code>.
     */
    private static GLFWErrorCallback errorCallback
            = GLFWErrorCallback.createPrint(System.err);

    /**
     * This key callback will check if ESC is pressed and will close the window
     * if it is pressed.
     */
    private static GLFWKeyCallback keyCallback = new GLFWKeyCallback() {

        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
                glfwSetWindowShouldClose(window, GLFW_TRUE);
            }
        }
    };

    /**
     * The main function will create a 640x480 window and renders a rotating
     * triangle until the window gets closed.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        long window;
        /* Set the error callback */
        glfwSetErrorCallback(errorCallback);

        /* Initialize GLFW */
        if (glfwInit() != GLFW_TRUE) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }

        /* Create window */
        window = glfwCreateWindow(640, 480, "Simple example", NULL, NULL);
        if (window == NULL) {
            glfwTerminate();
            throw new RuntimeException("Failed to create the GLFW window");
        }

        /* Center the window on screen */
        GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(window,
                (vidMode.width() - 640) / 2,
                (vidMode.height() - 480) / 2
        );

        /* Create OpenGL context */
        glfwMakeContextCurrent(window);
        GL.createCapabilities();

        /* Enable vertical synchronization */
        glfwSwapInterval(1);

        /* Set the key callback */
        glfwSetKeyCallback(window, keyCallback);

        /* Declare buffers for using inside the loop */
        IntBuffer width = BufferUtils.createIntBuffer(1);
        IntBuffer height = BufferUtils.createIntBuffer(1);
        float rtri = 0;
        float rquad = 0;
        /* Loop until window gets closed */
        while (glfwWindowShouldClose(window) != GLFW_TRUE) {
            float ratio;

            /* Get width and height to calcualte the ratio */
            glfwGetFramebufferSize(window, width, height);
            ratio = width.get() / (float) height.get();

            /* Rewind buffers for next get */
            width.rewind();
            height.rewind();

            /* Set viewport and clear screen */
            glViewport(0, 0, width.get(), height.get());
            glClear(GL_COLOR_BUFFER_BIT);

            /* Set ortographic projection */
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(-10, 10, -10f, 10f, 10f, -10f);
            
            glMatrixMode(GL_MODELVIEW);

            /* Rotate matrix */
            glLoadIdentity();
            //glRotatef((float) glfwGetTime() * 80f, 80f, 200f, f);

            /* Render triangle */
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
            glLoadIdentity();                   // Reset The View
            glTranslatef(-1.5f,0.0f,-6.0f);             // Move Left And Into The Screen
         
            glRotatef(rtri,0.0f,1.0f,0.0f);             // Rotate The Pyramid On It's Y Axis
         
            glBegin(GL_TRIANGLES);
            glColor3f(1.0f,0.0f,0.0f);          // Red
            glVertex3f( 0.0f, 1.0f, 0.0f);          // Top Of Triangle (Front)
            glColor3f(0.0f,1.0f,0.0f);          // Green
            glVertex3f(-1.0f,-1.0f, 1.0f);          // Left Of Triangle (Front)
            glColor3f(1f,1.0f,1.0f);          // Blue
            glVertex3f( 1.0f,-1.0f, 1.0f);
            glColor3f(1.0f,0.0f,0.0f);          // Red
            glVertex3f( 0.0f, 1.0f, 0.0f);          // Top Of Triangle (Right)
            glColor3f(0.0f,0.0f,1.0f);          // Blue
            glVertex3f( 1.0f,-1.0f, 1.0f);          // Left Of Triangle (Right)
            glColor3f(0.0f,1.0f,0.0f);          // Green
            glVertex3f( 1.0f,-1.0f, -1.0f);
            glColor3f(1.0f,0.0f,0.0f);          // Red
            glVertex3f( 0.0f, 1.0f, 0.0f);          // Top Of Triangle (Back)
            glColor3f(0.0f,1.0f,0.0f);          // Green
            glVertex3f( 1.0f,-1.0f, -1.0f);         // Left Of Triangle (Back)
            glColor3f(0.0f,0.0f,1.0f);          // Blue
            glVertex3f(-1.0f,-1.0f, -1.0f);
            glColor3f(1.0f,0.0f,0.0f);          // Red
            glVertex3f( 0.0f, 1.0f, 0.0f);          // Top Of Triangle (Left)
            glColor3f(0.0f,0.0f,1.0f);          // Blue
            glVertex3f(-1.0f,-1.0f,-1.0f);          // Left Of Triangle (Left)
            glColor3f(0.0f,1.0f,0.0f);          // Green
            glVertex3f(-1.0f,-1.0f, 1.0f);          // Right Of Triangle (Left)
            glEnd();
            glLoadIdentity();
            glTranslatef(1.5f,0.0f,-7.0f);              // Move Right And Into The Screen
             
            glRotatef(rquad,1.0f,1.0f,1.0f);            // Rotate The Cube On X, Y & Z
             
            glBegin(GL_QUADS);
            glColor3f(0.0f,1.0f,0.0f);          // Set The Color To Green
            glVertex4f( 1.0f, 1.0f,-1.0f, 1f);          // Top Right Of The Quad (Top)
            glVertex4f(-1.0f, 1.0f,-1.0f, 1f);          // Top Left Of The Quad (Top)
            glVertex4f(-1.0f, 1.0f, 1.0f, 1f);          // Bottom Left Of The Quad (Top)
            glVertex4f( 1.0f, 1.0f, 1.0f, 1f);
            glColor3f(1.0f,0.5f,0.0f);          // Set The Color To Orange
            glVertex4f( 1.0f,-1.0f, 1.0f, 1f);          // Top Right Of The Quad (Bottom)
            glVertex4f(-1.0f,-1.0f, 1.0f, 1f);          // Top Left Of The Quad (Bottom)
            glVertex4f(-1.0f,-1.0f,-1.0f, 1f);          // Bottom Left Of The Quad (Bottom)
            glVertex4f( 1.0f,-1.0f,-1.0f, 1f);
            glColor3f(1.0f,0.0f,0.0f);          // Set The Color To Red
            glVertex4f( 1.0f, 1.0f, 1.0f, 1f);          // Top Right Of The Quad (Front)
            glVertex4f(-1.0f, 1.0f, 1.0f, 1f);          // Top Left Of The Quad (Front)
            glVertex4f(-1.0f,-1.0f, 1.0f, 1f);          // Bottom Left Of The Quad (Front)
            glVertex4f( 1.0f,-1.0f, 1.0f, 1f);
            glColor3f(1.0f,1.0f,0.0f);          // Set The Color To Yellow
            glVertex4f( 1.0f,-1.0f,-1.0f, 1f);          // Bottom Left Of The Quad (Back)
            glVertex4f(-1.0f,-1.0f,-1.0f, 1f);          // Bottom Right Of The Quad (Back)
            glVertex4f(-1.0f, 1.0f,-1.0f, 1f);          // Top Right Of The Quad (Back)
            glVertex4f( 1.0f, 1.0f,-1.0f, 1f);
            glColor3f(0.0f,0.0f,1.0f);          // Set The Color To Blue
            glVertex4f(-1.0f, 1.0f, 1.0f, 1f);          // Top Right Of The Quad (Left)
            glVertex4f(-1.0f, 1.0f,-1.0f, 1f);          // Top Left Of The Quad (Left)
            glVertex4f(-1.0f,-1.0f,-1.0f, 1f);          // Bottom Left Of The Quad (Left)
            glVertex4f(-1.0f,-1.0f, 1.0f, 1f);
            glColor3f(1.0f,0.0f,1.0f);          // Set The Color To Violet
            glVertex4f( 1.0f, 1.0f,-1.0f, 1f);          // Top Right Of The Quad (Right)
            glVertex4f( 1.0f, 1.0f, 1.0f, 1f);          // Top Left Of The Quad (Right)
            glVertex4f( 1.0f,-1.0f, 1.0f, 1f);          // Bottom Left Of The Quad (Right)
            glVertex4f( 1.0f,-1.0f,-1.0f, 1f);          // Bottom Right Of The Quad (Right)
            glEnd();                        // Done Drawing The Quad
     
            rtri+=5f;                     // Increase The Rotation Variable For The Triangle
            rquad-=5f;
            /* Swap buffers and poll Events */
            glfwSwapBuffers(window);
            glfwPollEvents();

            /* Flip buffers for next loop */
            width.flip();
            height.flip();
        }

        /* Release window and its callbacks */
        glfwDestroyWindow(window);
        keyCallback.release();

        /* Terminate GLFW and release the error callback */
        glfwTerminate();
        errorCallback.release();
    }
}