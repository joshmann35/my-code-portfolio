
package test;

import java.util.ArrayList;

public class Chap6Test
{
	
	// QUESTION # 1
	public static double average(int[] a)
	{
		int total = 0;
		for(int i = 0; i < a.length; i++)
		{
			total += a[i];
		}
		double average = total / a.length;
		return average;
	}
	
	
	// QUESTION # 2
	public static void swap(String a, String b)
	{
		String temp = a;
		a = b;
		b = temp;
	}
	
	
	// QUESTION # 5
	public static void twoDArray()
	{
		int[][] a = new int[10][10];
		for(int i = 0; i < a.length; i++)
		{
			for(int j = 0; j < a[i].length; j++)
			{
				a[i][j] = i*j;
			}
		}
	}
	
	
	// QUESTION # 6
	public static void foreach()
	{
		char[] a = {'a', 'b', 'c', 'd', 'e', 'f'};
		for(char i: a)
		{
			System.out.print(i+" ");
		}
	}
	
	
	// QUESTION # 7
	public static void arraylist()
	{
		ArrayList a = new ArrayList();
		for(int i = 1; i <= 10; i++)
		{
			a.add(i);
		}
	}
}
