
package SortPractice;

public class CarRunner
{
	public static void main(String[] args)
	{
		Car[] lot = new Car[6];
		lot[0]=new Car("Ford", "Escape", "Red");
		lot[1]=new Car("Ford", "F150", "Green");
		lot[2]=new Car("Ram", "1500 Bighorn", "Silver");
		lot[3]=new Car("Jeep", "Cherokee", "Blue");
		lot[4]=new Car("Honda", "Pilot", "Black");
		lot[5]=new Car("Acura", "Legend", "White");
		System.out.println("Before Sorting");
		for(int i = 0; i < lot.length; i++)
		{
			System.out.println(lot[i]);
		}
		Sorts.insertionSort(lot);
		System.out.println("After Sorting");
		for(int i = 0; i < lot.length; i++)
		{
			System.out.println(lot[i]);
		}
	}
}
