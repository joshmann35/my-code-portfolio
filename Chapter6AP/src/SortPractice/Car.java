
package SortPractice;

import java.awt.Color;

public class Car implements Comparable
{
	
	private String make;
	private String model;
	private String color;

	public Car(String make, String model, String color)
	{
		this.make=make;
		this.model=model;
		this.color=color;
	}
	
	public String toString()
	{
		return this.make+" "+this.model+" "+this.color;
	}
	
	@Override
	public int compareTo(Object o)
	{
		Car temp = (Car)o;
		int result=this.make.compareTo(temp.getMake());
		if(result==0)
		{
			result=this.model.compareTo(temp.getModel());
		}
		return result;
	}

	private String getModel()
	{
		return this.model;
	}

	private String getMake()
	{
		return this.make;
	}

}
