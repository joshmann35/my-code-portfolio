package Examples;

import java.util.Random;
public class Switch {

	static int[] first=new int[10];
	static int[] second=new int[10];
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random numGen=new Random();
		
		
		//Generate Random Numbers
		genNumberList(numGen, first);
		genNumberList(numGen, second);
		
		//Output Original Lists
		outputList(first,1);
		outputList(second,2);
		
		switchThem();
		
		outputList(first, 1);
		outputList(second, 2);
		
	}

	private static void outputList(int[] list,int listNum) {
		if(listNum==1)
		System.out.print("First list: ");
		else
			System.out.print("Second list: ");
		for(int i:list)
		{
			System.out.print(i+" ");
		}
		System.out.println();
	}

	private static void switchThem() {
		int[] temp=new int[first.length];
		temp=first;
		first=second;
		second=temp;
		
		outputList(first,1);
		outputList(second,2);
		
		
	}

	private static void genNumberList(Random numGen, int[] first) {
		for(int i=0;i<first.length;i++)
			first[i]=numGen.nextInt(10)+1;
	}

}
