package Examples;

import java.util.Scanner;
public class ArrayPractice
{

	public static void main(String[] args)
	{
		// Array Practice
		
		int[] nums=new int[5];
		nums[0]=2;
		nums[1]=4;
		nums[2]=6;
		nums[3]=8;
		nums[4]=10;
		
		int total=0;
		int largest=nums[0];
		int smallest=nums[0];
		System.out.println(nums);
		
		for(int i=0; i<nums.length;i++)
		{
			//outputs all of the numbers in the array
			System.out.println(nums[i]);
			
			//accumulates a total of all of the elements of the array
			total+=nums[i];
			
			//finds the largest number in the array
			if (nums[i]>largest)
			{
				largest=nums[i];
			}
			
			//finds the smallest number in the array
			if (nums[i]<smallest)
			{
				smallest=nums[i];
			}
		}
		//calculate the average of the elements of the array
		double average=(double)total/nums.length;
		System.out.println("The average of the array elements is "+average);
		System.out.println("The largest number is "+largest);
		System.out.println("The smallest number is "+smallest);
		System.out.println("The sum of all of the elements in the array is "+total);
	}

}
