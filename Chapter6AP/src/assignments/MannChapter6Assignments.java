
package assignments;

import java.util.Scanner;

public class MannChapter6Assignments
{
	public static void main(String[] args)
	{
		// 6.1 and 6.2
		
		int[] nums = new int[51];
		for(int i = 0; i < 50; i++)
		{
			nums[(int)(Math.random()*51)]++;
		}
		for(int i = 0; i<nums.length; i++)
		{
			System.out.println((i-25)+":"+nums[i]);
		}
		
		
		// 6.4
		int[] numss = new int[10];
		for(int i = 0; i < 51; i++)
		{
			int temp = (int)(Math.random()*100)+1;
			if(temp>0&&temp<11) numss[0]++;
			else if(temp>10&&temp<21) numss[1]++;
			else if(temp>20&&temp<31) numss[2]++;
			else if(temp>30&&temp<41) numss[3]++;
			else if(temp>40&&temp<51) numss[4]++;
			else if(temp>50&&temp<61) numss[5]++;
			else if(temp>60&&temp<71) numss[6]++;
			else if(temp>70&&temp<81) numss[7]++;
			else if(temp>80&&temp<91) numss[8]++;
			else if(temp>90&&temp<101) numss[9]++;
			else
			{
				System.out.println("Invalid Number");
				i--;
			}
		}
		for(int i = 0; i < numss.length; i++)
		{
			System.out.print(((i*10)+1)+"\t - "+((i+1)*10)+"\t| ");
			for(int j = 0; j < numss[i]; j++)
			{
				System.out.print("*");
			}
			System.out.println();
		}
	}
}
