
package assignments.prob66;

public class prob66
{
	static Account[] accts= new Account[30];
	public static void main(String[] args)
	{
		accts[0] = new Account("Ted", 72354,102.56);
		accts[1] = new Account("Anita", 69713, 40.00);
		accts[2] = new Account("Sanchit", 93757, 759.32);
		accts[3] = new Account("Charly", 11211);
		accts[0].deposit(25.85);
		double gomezBalance = accts[1].deposit(500.00);
		System.out.println("Gomex balance after deposit: "+gomezBalance);
		System.out.println("Gomez after withdrawal: "+accts[1].withdraw(430.75, 1.50));
		accts[2].withdraw(800.00,0.0);
		accts[0].addInterest();
		accts[1].addInterest();
		accts[2].addInterest();
		System.out.println();
		System.out.println(accts[0]);
		System.out.println(accts[1]);
		System.out.println(accts[2]);
		
		System.out.println();
		
		accts[0].transfer(accts[1], 1000.00);
		
		System.out.println(accts[3]);
		
		add3PercentInterest();
		
	}
	
	public static void add3PercentInterest()
	{
		for(int i = 0; i < accts.length; i++)
		{
			if(accts[i]!=null)
				accts[i].addInterest(0.03);
		}
	}
	
}
