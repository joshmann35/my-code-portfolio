package assignments.prob66;
import java.text.NumberFormat;



public class Account
{
	private NumberFormat fmt = NumberFormat.getCurrencyInstance();
	
	private final double RATE = 0.035;
	
	private int acctNumber;
	private double balance;
	private String name;
	
	public Account (String owner, int account, double initial)
	{
		name = owner;
		acctNumber = account;
		balance = initial;
	}
	public Account(String owner, int account)
	{
		name=owner;
		acctNumber=account;
		balance=0;
	}
	public double deposit(double amount)
	{
		if(amount < 0)
		{
			System.out.println();
			System.out.println("error: Deposit Amount is invalid.");
			System.out.println(acctNumber + " "+ fmt.format(amount));
		}
		else
			balance += amount;
		return balance;
	}
	
	public double withdraw (double amount, double fee)
	{
		amount+= fee;
		if(amount<0)
		{
			System.out.println();
			System.out.println("Error: Withdraw amount is invalid");
			System.out.println("Account: "+acctNumber);
			System.out.println("Requested: "+fmt.format(amount));
			
		}
		else 
			if(amount > balance)
			{
				System.out.println();
				System.out.println("Error: Insufficient Funds");
				System.out.println("Account: "+acctNumber);
				System.out.println("Requested: "+fmt.format(amount));
				System.out.println("Available: "+fmt.format(balance));
				
			}
			else
				balance-=amount;
		return balance;
		
	}
	
	public double transfer(Account a, double amount)
	{
		System.out.println("Transfer Test");
		System.out.println(this);
		System.out.println(a);
		if(amount<0||amount>balance)
		{
			System.out.println();
			System.out.println("Error: Insufficient Funds || Invalid Input");
			System.out.println("Account: "+acctNumber);
			System.out.println("Requested: "+fmt.format(amount));
			System.out.println("Available: "+fmt.format(balance));
		}
		else
		{
			this.withdraw(amount, 0.00);
			a.deposit(amount);
			System.out.println("Transaction Completed");
			System.out.println(this);
			System.out.println(a);
		}
		return balance;
	}
	
	public double addInterest()
	{
		balance+=(balance*RATE);
		return balance;
	}
	public double addInterest(double rate)
	{
		balance+=(balance*rate);
		return balance;
	}
	
	public double getBalance()
	{
		return balance;
	}
	public int getAccountNumber()
	{
		return acctNumber;
	}
	public String toString()
	{
		return(acctNumber+"\t"+name+"\t"+fmt.format(balance));
	}
}
