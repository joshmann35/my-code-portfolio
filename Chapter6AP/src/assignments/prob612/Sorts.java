
package assignments.prob612;

public class Sorts
{
	public static void selectionSort(int[] numbers)
	{
		int min,temp;
		for(int i = 0;i<numbers.length-1;i++)
		{
			min=i;
			for(int s = i+1; s < numbers.length-1; s++)
			{
				if(numbers[s]<numbers[min])
					min=s;
				temp = numbers[min];
				numbers[min]=numbers[i];
				numbers[i]=temp;
			}
		}
	}
	public static void selectionSort(Comparable[] objects)
	{
		int min;
		Comparable temp;
		for(int i = 0;i<objects.length;i++)
		{
			min=i;
			for(int s = i+1; s < objects.length; s++)
			{
				if(objects[s].compareTo(objects[min])<0)
					min=s;
				
			}
			temp = objects[min];
			objects[min]=objects[i];
			objects[i]=temp;
		}
	}
	public static void insertionSort(int[] numbers)
	{
		for(int i = 1; i < numbers.length; i++)
		   {
			   int key = numbers[i];
			   int p = i;
			   while(p>0&&numbers[p-1]>key)
			   {
				   numbers[p]=numbers[p-1];
				   p--;
			   }
			   numbers[p]= key;
		   }
	}
	public static void insertionSort(Comparable[] objects)
	{
		for(int i = 1; i < objects.length; i++)
		   {
			   Comparable key = objects[i];
			   int p = i;
			   while(p>0&&objects[p-1].compareTo(key)>0)
			   {
				   objects[p]=objects[p-1];
				   p--;
			   }
			   objects[p]= key;
		   }
	}
}
