
package assignments.prob613;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class SelectionSortGUI extends Applet
{
	private int[] list;
	//private boolean first;
	private int p;
	public void init()
	{
		startNew();
	}
	
	public void startNew()
	{
		p = 0;
		//first = true;
		list = new int[10];
		for(int i = 0; i < list.length; i++)
		{
			list[i]=(int)(Math.random()*12)+1;
		}
		setSize(750,750);
		repaint();
		//doSort();
	}
	
	public void paint(Graphics g)
	{
		//if(first)
		//{
//			for(int i = 0; i < list.length; i++)
//			{
//				if(i==p)g.setColor(Color.RED);
//				else g.setColor(Color.BLACK);
//				g.fillRect(10, (i*50)+(i*10), list[i]*50, 50);
//			}
			//first = false;
			//try{Thread.sleep(1000);} catch (InterruptedException e){}
			//repaint();
		//}
		if(p < list.length-1)
		{
			int min=p;
			for(int s = p+1; s < list.length; s++)
			{
				if(list[s]<list[min])
				{
					min=s;
				}
				for(int i = 0; i < list.length; i++)
				{
					if(i==p)g.setColor(Color.RED);
					else if(i==min)g.setColor(Color.GREEN);
					else if(i==s)g.setColor(Color.BLUE);
					else g.setColor(Color.BLACK);
					g.fillRect(10, (i*50)+(i*10), list[i]*50, 50);
				}
				try{Thread.sleep(500);} catch (InterruptedException e){}
			}
			int temp = list[min];
			list[min]=list[p];
			list[p]=temp;
			
			p++;
			//first=true;
			repaint();
		}
		else
		{
			for(int i = 0; i < list.length; i++)
			{
				g.fillRect(10, (i*50)+(i*10), list[i]*50, 50);
			}
			try{Thread.sleep(1000);} catch (InterruptedException e){}
			startNew();
		}
		
		
		
	}
}
