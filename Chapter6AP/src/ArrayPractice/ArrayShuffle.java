
package ArrayPractice;

public class ArrayShuffle
{
	public static void main(String[] args)
	{
		int[] rand = new int[5];
		for(int i = 0; i < rand.length; i++)
		{
			rand[i] = (int)((Math.random()*100)+1);
			System.out.print(rand[i]+ " ");
		}
		for(int i = 0; i < rand.length; i++)
		{
			int randPos = (int)(Math.random()*5);
			int temp = rand[i];
			rand[i]=rand[randPos];
			rand[randPos]=temp;
		}
		System.out.println();
		for(int i = 0; i < rand.length; i++)
		{
			System.out.print(rand[i]+" ");
		}
	}
}
