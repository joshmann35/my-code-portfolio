
package mouseMaze.GUI;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.List;

import javax.swing.JComponent;

import mouseMaze.reference.Reference;

public class MouseMazeComp extends JComponent
{
	private List<Rectangle> maze;
	private int m_mode, f_mode;
	private int GRID_SCALE, GRID_WIDTH, GRID_HEIGHT;
	public MouseMazeComp(List<Rectangle> walls, int m_mode, int f_mode)
	{
		this.m_mode=m_mode;
		this.f_mode=f_mode;
		this.maze=walls;
		GRID_SCALE = Reference.BEGINNER_GRID_SCALE;
		GRID_HEIGHT = Reference.BEGINNER_GRID_HEIGHT;
		GRID_WIDTH = Reference.BEGINNER_GRID_WIDTH;
		switch(m_mode)
		{
		case Reference.BEGINNER_DIFFICULTY_VAL:
			GRID_SCALE = Reference.BEGINNER_GRID_SCALE;
			GRID_HEIGHT = Reference.BEGINNER_GRID_HEIGHT;
			GRID_WIDTH = Reference.BEGINNER_GRID_WIDTH;
			break;
		case Reference.EASY_DIFFICULTY_VAL:
			GRID_SCALE = Reference.EASY_GRID_SCALE;
			GRID_HEIGHT = Reference.EASY_GRID_HEIGHT;
			GRID_WIDTH = Reference.EASY_GRID_WIDTH;
			break;
		case Reference.MEDIUM_DIFFICULTY_VAL:
			GRID_SCALE = Reference.MEDIUM_GRID_SCALE;
			GRID_HEIGHT = Reference.MEDIUM_GRID_HEIGHT;
			GRID_WIDTH = Reference.MEDIUM_GRID_WIDTH;
			break;
		case Reference.HARD_DIFFICULTY_VAL:
			GRID_SCALE = Reference.HARD_GRID_SCALE;
			GRID_HEIGHT = Reference.HARD_GRID_HEIGHT;
			GRID_WIDTH = Reference.HARD_GRID_WIDTH;
			break;
		}
	}
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		paintSF(g2);
		paintWalls(g2);
		paintFOG(g2);
	}
	private void paintSF(Graphics2D g2)
	{
		g2.setColor(Reference.START_COLOR);
		g2.fill(new Rectangle(0,0,GRID_SCALE, GRID_SCALE));
		g2.setColor(Reference.FINISH_COLOR);
		g2.fill(new Rectangle((GRID_WIDTH-1)*GRID_SCALE,(GRID_HEIGHT-1)*GRID_SCALE,GRID_SCALE, GRID_SCALE));
	}
	private void paintFOG(Graphics2D g2)
	{
		g2.setColor(Reference.FOG_COLOR);
		switch(f_mode)
		{
		case Reference.BEGINNER_DIFFICULTY_VAL: 
			g2.setStroke(new BasicStroke((int)(2000*Reference.BEGINNER_FOG_INTENSITY)));
			break;
		case Reference.EASY_DIFFICULTY_VAL: 
			g2.setStroke(new BasicStroke((int)(2000*Reference.EASY_FOG_INTENSITY)));
			break;
		case Reference.MEDIUM_DIFFICULTY_VAL: 
			g2.setStroke(new BasicStroke((int)(2000*Reference.MEDIUM_FOG_INTENSITY)));
			break;
		case Reference.HARD_DIFFICULTY_VAL: 
			g2.setStroke(new BasicStroke((int)(2000*Reference.HARD_FOG_INTENSITY)));
			break;
		}
		try
		{
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2.drawOval(((int)this.getMousePosition().getX())-1000, ((int)this.getMousePosition().getY())-1000, 2000, 2000);
		}
		catch(NullPointerException e){}
	}
	private void paintWalls(Graphics2D g2)
	{
		g2.setColor(Reference.WALL_COLOR);
		for(int i = 0; i < maze.size(); i++)
		{
			g2.fill(maze.get(i));
		}
	}
	public List<Rectangle> getMaze()
	{
		return maze;
	}
	public void setMaze(List<Rectangle> maze)
	{
		this.maze = maze;
	}
}
