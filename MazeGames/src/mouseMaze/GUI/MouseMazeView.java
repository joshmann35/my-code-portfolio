
package mouseMaze.GUI;



import java.awt.AWTException;
import java.awt.Cursor;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import mouseMaze.reference.Reference;

public class MouseMazeView implements MouseListener, KeyListener
{
	private JFrame frame;
	private MouseMazeComp panel;
	private int[][] maze;
	private List<Rectangle> walls;
	private int x;
	private int y;
	private int m_mode, f_mode;
	private int GRID_HEIGHT;
	private int GRID_WIDTH;
	private int GRID_SCALE;
	private boolean isSolving, isEnd = false;
	private Timer timer = new Timer(Reference.CYCLE_TIME, new ActionListener()
	{
		public void actionPerformed(ActionEvent e)
		{
			panel.repaint();
			if (isSolving)
			{
				checkMousePos();
			}
		}
	});
	public MouseMazeView(JFrame frameIn, int m_mode, int f_mode)
	{
		this.m_mode=m_mode;
		this.f_mode=f_mode;
		GRID_SCALE = Reference.BEGINNER_GRID_SCALE;
		GRID_HEIGHT = Reference.BEGINNER_GRID_HEIGHT;
		GRID_WIDTH = Reference.BEGINNER_GRID_WIDTH;
		switch(m_mode)
		{
		case Reference.BEGINNER_DIFFICULTY_VAL:
			GRID_SCALE = Reference.BEGINNER_GRID_SCALE;
			GRID_HEIGHT = Reference.BEGINNER_GRID_HEIGHT;
			GRID_WIDTH = Reference.BEGINNER_GRID_WIDTH;
			break;
		case Reference.EASY_DIFFICULTY_VAL:
			GRID_SCALE = Reference.EASY_GRID_SCALE;
			GRID_HEIGHT = Reference.EASY_GRID_HEIGHT;
			GRID_WIDTH = Reference.EASY_GRID_WIDTH;
			break;
		case Reference.MEDIUM_DIFFICULTY_VAL:
			GRID_SCALE = Reference.MEDIUM_GRID_SCALE;
			GRID_HEIGHT = Reference.MEDIUM_GRID_HEIGHT;
			GRID_WIDTH = Reference.MEDIUM_GRID_WIDTH;
			break;
		case Reference.HARD_DIFFICULTY_VAL:
			GRID_SCALE = Reference.HARD_GRID_SCALE;
			GRID_HEIGHT = Reference.HARD_GRID_HEIGHT;
			GRID_WIDTH = Reference.HARD_GRID_WIDTH;
			break;
		}
		maze = new int[GRID_HEIGHT][GRID_WIDTH];
		walls = new ArrayList<Rectangle>();
		frame = frameIn;
		panel = new MouseMazeComp(walls, m_mode, f_mode);
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setSize((GRID_SCALE*GRID_WIDTH)+7+GRID_SCALE/3, (GRID_SCALE*GRID_HEIGHT)+29+GRID_SCALE/3);
		frame.setResizable(false);
		frame.setFocusable(true);
		frame.requestFocusInWindow();
		frame.addMouseListener(this);
		frame.addKeyListener(this);
		frame.setCursor(new Cursor(0));
		frame.setVisible(true);
		startGame();
	}
	private void startGame()
	{
		x = GRID_WIDTH;
		y = GRID_HEIGHT;
		generateMaze(0,0);
		generateWalls();
		panel.setMaze(walls);
		panel.repaint();
		Robot r;
		try
		{
			r = new Robot();
			Insets i = frame.getInsets();
			frame.setSize((GRID_SCALE*GRID_WIDTH)+i.left+i.right+GRID_SCALE/8, (GRID_SCALE*GRID_HEIGHT)+i.top+i.bottom+GRID_SCALE/8);
			r.mouseMove(frame.getX()+i.left+GRID_SCALE/2+GRID_SCALE/8, frame.getY()+i.top+GRID_SCALE/2+GRID_SCALE/8);
			
		} catch (AWTException e)
		{
			e.printStackTrace();
		}
		isSolving=true;
		timer.start();
		
	}
	private void checkMousePos()
	{
		for(int i = 0; i < walls.size(); i++)
		{
			try
			{
				if(walls.get(i).intersects(panel.getMousePosition().getX(), panel.getMousePosition().getY(), 1, 1))
				{
					isSolving = false;
					JOptionPane.showMessageDialog(frame, Reference.FAIL_MESSAGE);
				}				
			}
			catch(NullPointerException e){}
		}
		try
		{
		if(panel.getMousePosition().getX()<0||panel.getMousePosition().getY()<0||panel.getMousePosition().getX()>(GRID_SCALE*GRID_WIDTH)||panel.getMousePosition().getY()>(GRID_SCALE*GRID_HEIGHT))
		{
			JOptionPane.showMessageDialog(frame, Reference.FAIL_MESSAGE);
			isSolving = false;
		}
		else if((panel.getMousePosition().getX()<(GRID_SCALE*GRID_WIDTH)&&panel.getMousePosition().getY()<(GRID_SCALE*GRID_HEIGHT))&&(panel.getMousePosition().getX()>(GRID_SCALE*(GRID_WIDTH-1))&&panel.getMousePosition().getY()>(GRID_SCALE*(GRID_HEIGHT-1))))
		{
			JOptionPane.showMessageDialog(frame, Reference.WIN_MESSAGE);
			isSolving = false;
		}
		}
		catch(NullPointerException e){}
		if(!isSolving)
		{
			frame.getContentPane().remove(panel);
			isEnd = true;
		}
	}
	private void generateWalls()
	{
		for (int i = 0; i < y; i++) {
			// draw the north edge
			for (int j = 0; j < x; j++) {
				if((maze[j][i] & 1)==0)
				{
					walls.add(new Rectangle((j*GRID_SCALE),(i*GRID_SCALE),(GRID_SCALE),(GRID_SCALE)/8));
				}
				else
				{
					walls.add(new Rectangle((j*GRID_SCALE),(i*GRID_SCALE),(GRID_SCALE)/8,(GRID_SCALE)/8));
				}
				
			}
			walls.add(new Rectangle((GRID_WIDTH*GRID_SCALE),(i*GRID_SCALE),(GRID_SCALE)/8,(GRID_SCALE)));
			// draw the west edge
			for (int j = 0; j < x; j++) {
				if((maze[j][i]&8)==0)
				{
					walls.add(new Rectangle((j*GRID_SCALE),(i*GRID_SCALE),(GRID_SCALE)/8,(GRID_SCALE)));
				}
			}
		}
		// draw the bottom line
		for (int j = 0; j < x; j++) {
			walls.add(new Rectangle((j*GRID_SCALE),(x*GRID_SCALE),(GRID_SCALE),(GRID_SCALE)/8));
		}
		walls.add(new Rectangle((GRID_WIDTH*GRID_SCALE),(GRID_HEIGHT*GRID_SCALE),(GRID_SCALE)/8,(GRID_SCALE)/8));
	}
	private void generateMaze(int cx, int cy) {
		DIR[] dirs = DIR.values();
		Collections.shuffle(Arrays.asList(dirs));
		for (DIR dir : dirs) {
			int nx = cx + dir.dx;
			int ny = cy + dir.dy;
			if (between(nx, x) && between(ny, y)
					&& (maze[nx][ny] == 0)) {
				maze[cx][cy] |= dir.bit;
				maze[nx][ny] |= dir.opposite.bit;
				generateMaze(nx, ny);
			}
		}
	}
 
	private static boolean between(int v, int upper) {
		return (v >= 0) && (v < upper);
	}
 
	private enum DIR {
		N(1, 0, -1), S(2, 0, 1), E(4, 1, 0), W(8, -1, 0);
		private final int bit;
		private final int dx;
		private final int dy;
		private DIR opposite;
 
		// use the static initializer to resolve forward references
		static {
			N.opposite = S;
			S.opposite = N;
			E.opposite = W;
			W.opposite = E;
		}
 
		private DIR(int bit, int dx, int dy) {
			this.bit = bit;
			this.dx = dx;
			this.dy = dy;
		}
	};
	
	public boolean getIsEnd()
	{
		return isEnd;
	}
	
	@Override
	public void keyPressed(KeyEvent e)
	{
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
		{
			frame.getContentPane().remove(panel);
			isEnd = true;
		}
		
	}
	@Override public void keyReleased(KeyEvent arg0){}
	@Override public void keyTyped(KeyEvent arg0){}
	@Override public void mouseClicked(MouseEvent arg0){}
	@Override public void mouseEntered(MouseEvent arg0){}
	@Override public void mouseExited(MouseEvent arg0){}
	@Override public void mousePressed(MouseEvent arg0){}
	@Override public void mouseReleased(MouseEvent arg0){}
}
