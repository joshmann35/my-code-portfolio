
package mouseMaze.reference;

import java.awt.Color;

public class Reference
{
	public static final String FRAME_TITLE = "Mouse Maze";
	
	public static final int CYCLE_TIME = 1; //Default 10
	
	// HARD MODE
	public static final int HARD_GRID_SCALE = 25; // Default 25
	public static final int HARD_GRID_WIDTH = 30; // Default 30
	public static final int HARD_GRID_HEIGHT = 30; // Default 30
	public static final double HARD_FOG_INTENSITY = 0.9; // Deafault 0.9
	
	// MEDIUM MODE
	public static final int MEDIUM_GRID_SCALE = 50; // Default 50
	public static final int MEDIUM_GRID_WIDTH = 15; // Default 15
	public static final int MEDIUM_GRID_HEIGHT = 15; // Default 15
	public static final double MEDIUM_FOG_INTENSITY = 0.75; // Deafault 0.75
	
	// EASY MODE
	public static final int EASY_GRID_SCALE = 75; // Default 75
	public static final int EASY_GRID_WIDTH = 10; // Default 10
	public static final int EASY_GRID_HEIGHT = 10; // Default 10
	public static final double EASY_FOG_INTENSITY = 0.5; // Deafault 0.5
	
	// BEGINNER MODE
	public static final int BEGINNER_GRID_SCALE = 100; // Default 100
	public static final int BEGINNER_GRID_WIDTH = 5; // Default 5
	public static final int BEGINNER_GRID_HEIGHT = 5; // Default 5
	public static final double BEGINNER_FOG_INTENSITY = 0.0; // Deafault 0.0
	
	
	public static final String BEGINNER_DIFFICULTY = "Beginner";
	public static final String EASY_DIFFICULTY = "Easy";
	public static final String MEDIUM_DIFFICULTY = "Medium";
	public static final String HARD_DIFFICULTY = "Hard";
	
	public static final int BEGINNER_DIFFICULTY_VAL = 0;
	public static final int EASY_DIFFICULTY_VAL = 1;
	public static final int MEDIUM_DIFFICULTY_VAL = 2;
	public static final int HARD_DIFFICULTY_VAL = 3;
	
	public static final String STARTING_MESSAGE = "Move Your Mouse From the\nStart to the Finish";
	public static final String FAIL_MESSAGE = "You Lose";
	public static final String WIN_MESSAGE = "You WIN!";
	
	public static final Color START_COLOR = Color.GREEN;
	public static final Color FINISH_COLOR = Color.RED;
	public static final Color WALL_COLOR = Color.BLACK;
	public static final Color FOG_COLOR = Color.BLACK;
}
