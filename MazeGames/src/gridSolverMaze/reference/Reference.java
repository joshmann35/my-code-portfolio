
package gridSolverMaze.reference;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Reference
{
	/*
	 	MAIN
	 */
	
	public static final String FRAME_TITLE = "Grid Maze";
	
	public static final int CYCLE_TIME = 10; //Default 10
	public static final int UNIT_CYCLE_TIME = 100; // Default 100
	
	public static final int GRID_SCALE = 25; // Default 50
	public static final int GRID_WIDTH = 30; // Default 15
	public static final int GRID_HEIGHT = 30; // Default 15
	
	public static final String STARTING_MESSAGE = "Left Click - Place Block\nRight Click - Remove Block\nScroll to Change Size of Selection\nSpace - Start/Reset\nF - Fill\nC - Clear\nL - Line Follow\n` - Debug Mode";
	public static final String COMPLETION_MESSAGE = "Maze Completed";
	public static final String FAILURE_MESSAGE = "The Maze Provided Cannot Be Solved";
	
	/*
	 	GUI
	 */
	public static final Color WALL_COLOR = Color.BLACK; // Default Black
	public static final Color UNIT_COLOR = Color.BLUE; // Default Blue
	public static final Color LINE_COLOR = Color.GREEN; // Default Green
	public static final Color GRID_COLOR = Color.BLACK; // Default Black
	public static final Color START_COLOR = Color.GREEN; // Default Green
	public static final Color FINISH_COLOR = Color.RED; // Default Red
	public static final Color MOUSE_POS_COLOR = Color.MAGENTA;
		
	
	/*
	 	BUTTONS
	 */
	public static final int PLACE_WALL_BUTTON = MouseEvent.BUTTON1;  // Default = MouseEvent.BUTTON1  -  Left Mouse Click
	public static final int REMOVE_WALL_BUTTON = MouseEvent.BUTTON3;  // Default = MouseEvent.BUTTON3  -  Right Mouse Click
	public static final int DEBUG_MODE_KEY = 192;  // Default = 192  -  `
	public static final int LINE_DRAW_KEY = KeyEvent.VK_L;   // Default = KeyEvent.VK_L  -  l
	public static final int START_KEY = KeyEvent.VK_SPACE;   // Default = KeyEvent.VK_SPACE  -
	public static final int CLEAR_KEY = KeyEvent.VK_C; // Default C
	public static final int FILL_KEY = KeyEvent.VK_F; // Default F
	
	/*
	 	DEBUG STATEMENTS
	 */
	public static final String D_INVALID_WALL_PLACE = "CANNOT PLACE THERE";
	public static final String D_SOLVED = "SOLVED";
	public static final String D_FAILED = "NOT SOLVABLE";
	public static final String D_LINE_ENABLED = "ENABLED LINE DRAW";
	public static final String D_LINE_DISABLED = "DISABLED LINE DRAW";
	public static final String D_DEBUG_ENABLED = "DEBUG MODE ENABLED";
	public static final String D_DEBUG_DISABLED = "DEBUG MODE ENABLED";

	public static final int END_KEY = KeyEvent.VK_ESCAPE;
	
}
