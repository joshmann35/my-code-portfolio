
package gridSolverMaze.handler;

import gridSolverMaze.entity.Wall;
import gridSolverMaze.reference.Reference;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

public class Solver
{
	private List<Rectangle> walls;
	private List<Wall> spaces;
	private List<Wall> moves;
	private Rectangle unit;
	private boolean isSolved = false;
	public Solver(Rectangle unit, List<Rectangle> walls, List<Wall> spaces, List<Wall> moves)
	{
		this.walls=walls;
		this.unit=unit;
		this.spaces = spaces;
		this.moves=moves;
		makeSpaces();
		if(traverse(0,0))
		{
			isSolved = true;
		}
		else
		{
			isSolved = false;
		}
	}
	public boolean traverse (int row, int column)
	{
		boolean done = false;
		if(valid(row,column))
		{
			int temp = 0;
			for(int i = 0; i < spaces.size(); i++)
			{
				if(spaces.get(i).getX()==row&&spaces.get(i).getY()==column)
				{
					spaces.get(i).setTried(true);
				}
			}
			moves.add(new Wall(row*Reference.GRID_SCALE, column*Reference.GRID_SCALE, 0,0));
			if(row==Reference.GRID_HEIGHT-1&&column == Reference.GRID_WIDTH-1)
			{
				done = true;
			}
			else
			{
				done = traverse(row+1, column);
				if(!done)
				{
					done = traverse (row, column+1);
				}
				if(!done)
				{
					done = traverse (row-1, column);
				}
				if(!done)
				{
					done = traverse (row, column-1);
				}
			}			
		}
		return done;
	}
	
	private boolean valid (int row, int column)
	{
		for(int i = 0; i < walls.size(); i++)
		{
			if((walls.get(i).getX()/Reference.GRID_SCALE==row&&walls.get(i).getY()/Reference.GRID_SCALE==column))
			{
				return false;
			}
		}
		for(int i = 0; i < spaces.size(); i++)
		{
			if(spaces.get(i).getX()==row&&spaces.get(i).getY()==column)
			{
				if(spaces.get(i).getTried())
				{
					return false;
				}
			}
		}
		if((row>Reference.GRID_HEIGHT-1)||(column>Reference.GRID_WIDTH-1)||(row<0)||(column<0))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	private void makeSpaces()
	{
		for(int i = 0; i < Reference.GRID_WIDTH; i++)
		{
			for(int j = 0; j < Reference.GRID_HEIGHT; j++)
			{
				boolean hasWall = false;
				for(int k = 0; k < walls.size(); k++)
				{
					if((walls.get(k).getX()/Reference.GRID_SCALE==i&&walls.get(k).getY()/Reference.GRID_SCALE==j))
					{
						hasWall = true;
					}
				}
				if(!hasWall)
				{
					spaces.add(new Wall(i,j,0,0));
				}
			}
		}
	}
	
	public boolean getIsSolved()
	{
		return isSolved;
	}
}
