
package gridSolverMaze.GUI;

import gridSolverMaze.entity.Line;
import gridSolverMaze.reference.Reference;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.List;

import javax.swing.JComponent;

public class GridMazeComp extends JComponent
{
	private List<Rectangle> walls;
	private List<Line> lines;
	private Rectangle unit;
	private boolean isSolving, drawLine = false;
	private int mouseSel = 0;
	public GridMazeComp(Rectangle unit, List<Rectangle> walls, List<Line> lines)
	{
		this.walls = walls;
		this.unit = unit;
		this.lines=lines;
	}
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		paintWalls(g2);
		paintSF(g2);
		if(!isSolving)
		{
			paintGrid(g2);
			paintMousePos(g2);
		}
		if(isSolving)
		{
			paintUnit(g2);
			if(drawLine)
			{
				paintLine(g2);
			}
		}
	}
	public void paintMousePos(Graphics2D g2)
	{
		g2.setColor(Reference.MOUSE_POS_COLOR);
		try
		{
			g2.drawRect((int)((this.getMousePosition().getX()/Reference.GRID_SCALE)-mouseSel)*Reference.GRID_SCALE, (int)((this.getMousePosition().getY()/Reference.GRID_SCALE)-mouseSel)*Reference.GRID_SCALE, Reference.GRID_SCALE+(mouseSel*Reference.GRID_SCALE)*2, Reference.GRID_SCALE+(mouseSel*Reference.GRID_SCALE)*2);
		}catch(Exception e){}
	}
	public void paintWalls(Graphics2D g2)
	{
		g2.setColor(Reference.WALL_COLOR);
		for(int i = 0; i < walls.size(); i++)
		{
			g2.fill(walls.get(i));
		}
	}
	public void paintUnit(Graphics2D g2)
	{
		g2.setColor(Reference.UNIT_COLOR);
		g2.fill(unit);
	}
	public void paintLine(Graphics2D g2)
	{
		g2.setColor(Reference.LINE_COLOR);
		for(int i = 0; i < lines.size()-1; i++)
		{
			g2.drawLine(lines.get(i).getX1(), lines.get(i).getY1(), lines.get(i).getX2(), lines.get(i).getY2());
		}
	}
	public void paintGrid(Graphics2D g2)
	{
		g2.setColor(Reference.GRID_COLOR);
		for(int i = 0; i < this.getWidth(); i+=Reference.GRID_SCALE)
		{
			g2.drawLine(i, 0, i, this.getHeight());
		}
		for(int i = 0; i < this.getHeight(); i+=Reference.GRID_SCALE)
		{
			g2.drawLine(0, i, this.getWidth(), i);
		}
	}
	public void paintSF(Graphics2D g2)
	{
		g2.setColor(Reference.START_COLOR);
		g2.fill(new Rectangle(0,0,Reference.GRID_SCALE,Reference.GRID_SCALE));
		g2.setColor(Reference.FINISH_COLOR);
		g2.fill(new Rectangle(((Reference.GRID_WIDTH-1)*Reference.GRID_SCALE),((Reference.GRID_HEIGHT-1)*Reference.GRID_SCALE),Reference.GRID_SCALE,Reference.GRID_SCALE));
	}
	public void placeWall(int xpos, int ypos)
	{
		boolean p = true;
		for(int i = 0; i < walls.size(); i++)
		{
			if(walls.get(i).getX()==xpos&&walls.get(i).getY()==ypos)
			{
				p=false;
			}
		}
		if(p)
		{
			walls.add(new Rectangle(xpos, ypos, Reference.GRID_SCALE, Reference.GRID_SCALE));
		}
	}
	public void removeWall(int xpos, int ypos)
	{
		for(int i = 0; i < walls.size(); i++)
		{
			if(walls.get(i).getX()==xpos&&walls.get(i).getY()==ypos)
			{
				walls.remove(i);
			}
		}
	}
	public void setSolving(boolean solving)
	{
		isSolving = solving;
	}
	public void setDrawLine(boolean draw)
	{
		drawLine=draw;
	}
	public void setLines(List<Line> line)
	{
		this.lines=line;
	}
	public void setWalls(List<Rectangle> walls)
	{
		this.walls=walls;
	}
	public void setMouseSel(int ms)
	{
		mouseSel = ms;
	}
}
