package gridSolverMaze.GUI;

import gridSolverMaze.entity.Line;
import gridSolverMaze.entity.Wall;
import gridSolverMaze.handler.Solver;
import gridSolverMaze.reference.Reference;

import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class GridMazeView implements MouseListener, KeyListener, MouseWheelListener
{
	private JFrame frame;
	private GridMazeComp panel;
	private List<Rectangle> walls;
	private Rectangle unit;
	private boolean pw, rw, isSolving, isMoving, drawLine, debugMode, slvd = false;
	private List<Wall> spaces;
	private List<Wall> moves;
	private List<Line> lines;
	private int moveInt,mouseSel = 0;
	private boolean isEnd = false;

	private Timer timer = new Timer(Reference.CYCLE_TIME, new ActionListener()
	{
		public void actionPerformed(ActionEvent e)
		{
			if (!isSolving)
			{
				panel.repaint();
				if (pw)
					addWalls();
				if (rw)
					remWalls();
			}
		}
	});
	private Timer mover = new Timer(Reference.UNIT_CYCLE_TIME, new ActionListener()
	{
		public void actionPerformed(ActionEvent e)
		{
			if (isMoving && moveInt < moves.size())
			{
				moveUnit(moveInt);
				moveInt++;
			}
			else if(isMoving)
			{
				isSolving = false;
				panel.setSolving(false);
				isMoving = false;
				mover.stop();
				unit.x = 0;
				unit.y = 0;
				panel.repaint();
				spaces = new ArrayList<Wall>();
				moves = new ArrayList<Wall>();
				moveInt = 0;
				lines = new ArrayList<Line>();
				panel.setLines(lines);
				if(slvd)
					JOptionPane.showMessageDialog(frame, Reference.COMPLETION_MESSAGE);
				else
					JOptionPane.showMessageDialog(frame, Reference.FAILURE_MESSAGE);
			}
		}
	});

	public GridMazeView(JFrame inFrame)
	{
		frame = inFrame;
		walls = new ArrayList<Rectangle>();
		spaces = new ArrayList<Wall>();
		moves = new ArrayList<Wall>();
		lines = new ArrayList<Line>();
		unit = new Rectangle(0, 0, Reference.GRID_SCALE, Reference.GRID_SCALE);
		panel = new GridMazeComp(unit, walls, lines);
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setSize(Reference.GRID_SCALE*(Reference.GRID_HEIGHT)+7, Reference.GRID_SCALE*(Reference.GRID_WIDTH)+29);
		frame.setResizable(false);
		frame.setFocusable(true);
		frame.requestFocusInWindow();
		frame.addMouseListener(this);
		frame.addKeyListener(this);
		frame.addMouseWheelListener(this);
		frame.setCursor(new Cursor(0));
		JOptionPane.showMessageDialog(frame, Reference.STARTING_MESSAGE);
		frame.setVisible(true);
		timer.start();
	}

	@Override public void mouseClicked(MouseEvent arg0){}
	@Override public void mouseEntered(MouseEvent arg0){}
	@Override public void mouseExited(MouseEvent arg0){}

	@Override
	public void mousePressed(MouseEvent e)
	{
		if (debugMode)
		{
			System.err.println(e.getButton() + " - ( " + e.getX() + " , "
					+ e.getY() + " )");
		}
		if (e.getButton() == MouseEvent.BUTTON1 && !isSolving)
		{
			pw = true;
		} else if (e.getButton() == MouseEvent.BUTTON3 && !isSolving)
		{
			rw = true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		if (e.getButton() == Reference.PLACE_WALL_BUTTON)
		{
			pw = false;
		} else if (e.getButton() == Reference.REMOVE_WALL_BUTTON)
		{
			rw = false;
		}
	}

	public void addWalls()
	{
		try
		{
			double xpos = panel.getMousePosition().getX();
			double ypos = panel.getMousePosition().getY();
			int x = 0;
			int y = 0;
			for(int i = mouseSel*-1; i < (mouseSel)+1; i++)
			{
				for(int j = mouseSel*-1; j < (mouseSel)+1; j++)
				{
					x = ((int) ((xpos) / Reference.GRID_SCALE)+i) * Reference.GRID_SCALE;
					y = ((int) ((ypos) / Reference.GRID_SCALE)+j) * Reference.GRID_SCALE;
					if (debugMode)
						System.err.println("( " + x + " , " + y + " )");
					if ((x == Reference.GRID_SCALE*(Reference.GRID_WIDTH-1) && y == Reference.GRID_SCALE*(Reference.GRID_HEIGHT-1)) || (x == 0 && y == 0))
					{
						if(debugMode)
							System.err.println(Reference.D_INVALID_WALL_PLACE);
					} else
					{
						panel.placeWall(x, y);
					}
				}
			}
		} catch (NullPointerException e)
		{
		}
	}
	public void remWalls()
	{
		try
		{
			for(int i = mouseSel*-1; i < (mouseSel)+1; i++)
			{
				for(int j = mouseSel*-1; j < (mouseSel)+1; j++)
				{
					int x = ((int) ((panel.getMousePosition().getX()) / Reference.GRID_SCALE)+i) * Reference.GRID_SCALE;
					int y = ((int) ((panel.getMousePosition().getY()) / Reference.GRID_SCALE)+j) * Reference.GRID_SCALE;
					panel.removeWall(x, y);
				}
			}
		} catch (NullPointerException e){}
	}
	
	public boolean getIsEnd()
	{
		return isEnd;
	}
	
	@Override
	public void keyPressed(KeyEvent e)
	{
		if (debugMode)
		{
			System.err.println(e.getKeyCode()+" - "+e.getKeyChar());
		}
		if (e.getKeyCode() == Reference.START_KEY)
		{
			if (isSolving)
			{
				isSolving = false;
				panel.setSolving(false);
				isMoving = false;
				mover.stop();
				unit.x = 0;
				unit.y = 0;
				panel.repaint();
				spaces = new ArrayList<Wall>();
				moves = new ArrayList<Wall>();
				moveInt = 0;
				lines = new ArrayList<Line>();
				panel.setLines(lines);
			} 
			else
			{
				isSolving = true;
				panel.setSolving(true);
				Solver s = new Solver(unit, walls, spaces, moves);
				slvd = s.getIsSolved();
				if(debugMode)
				{
					if(slvd)
					{
						System.err.println(Reference.D_SOLVED);
					}
					else
					{
						System.err.println(Reference.D_FAILED);
					}
				}
				isMoving = true;
				mover.start();
			}
		} else if (e.getKeyCode() == Reference.LINE_DRAW_KEY)
		{
			if (!drawLine)
			{
				drawLine = true;
				panel.setDrawLine(true);
				if(debugMode)
					System.err.println(Reference.D_LINE_ENABLED);
			} else
			{
				drawLine = false;
				panel.setDrawLine(false);
				lines = new ArrayList<Line>();
				panel.setLines(lines);
				if(debugMode)
					System.err.println(Reference.D_LINE_DISABLED);
			}
		} else if (e.getKeyCode() == Reference.DEBUG_MODE_KEY)
		{
			if (debugMode)
			{
				System.err.println(Reference.D_DEBUG_DISABLED);
				debugMode = false;
			} else
			{
				System.err.println(Reference.D_DEBUG_ENABLED);
				debugMode = true;
			}
		}
		else if(e.getKeyCode() == Reference.CLEAR_KEY)
		{
			if(!isSolving)
			{
				walls = new ArrayList<Rectangle>();
				panel.setWalls(walls);
			}
		}
		else if(e.getKeyCode() == Reference.FILL_KEY)
		{
			if(!isSolving)
			{
				for(int i = 0; i < Reference.GRID_WIDTH; i++)
				{
					for(int j = 0; j < Reference.GRID_HEIGHT; j++)
					{
						if((i==0&&j==0)||(i==Reference.GRID_WIDTH-1&&j==Reference.GRID_HEIGHT-1))
						{
							
						}
						else
						{
							walls.add(new Rectangle(i*Reference.GRID_SCALE, j*Reference.GRID_SCALE, Reference.GRID_SCALE, Reference.GRID_SCALE));
						}
					}
				}
			}
		}
		else if(e.getKeyCode() == Reference.END_KEY)
		{
			frame.getContentPane().remove(panel);
			isEnd = true;
		}
	}

	@Override public void keyReleased(KeyEvent arg0){}
	@Override public void keyTyped(KeyEvent arg0){}

	public void moveUnit(int spot)
	{
		int xLine = unit.x + (int) (unit.getWidth() / 2);
		int yLine = unit.y + (int) (unit.getHeight() / 2);
		unit.x = (int) moves.get(spot).getX();
		unit.y = (int) moves.get(spot).getY();
		if (drawLine)
		{
			lines.add(new Line(xLine, yLine, unit.x
					+ (int) (unit.getWidth() / 2), unit.y
					+ (int) (unit.getHeight() / 2)));
		}
		panel.repaint();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e)
	{
		if((mouseSel==0&&e.getWheelRotation()==1)||(mouseSel==5&&e.getWheelRotation()==-1)){}
		else
		{
			mouseSel-=e.getWheelRotation();
			panel.setMouseSel(mouseSel);
		}
		
	}
}
