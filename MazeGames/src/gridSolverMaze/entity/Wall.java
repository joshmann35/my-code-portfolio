
package gridSolverMaze.entity;

import java.awt.Rectangle;

public class Wall extends Rectangle
{
	private boolean tried = false;
	public Wall(int x, int y, int w, int h)
	{
		super(x,y,w,h);
	}
	public void setTried(boolean t)
	{
		tried = t;
	}
	public boolean getTried()
	{
		return tried;
	}
}
