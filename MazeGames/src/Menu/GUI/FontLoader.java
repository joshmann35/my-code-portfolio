
package Menu.GUI;

import java.awt.Font;
import java.io.BufferedInputStream;
import java.io.InputStream;

public class FontLoader
{
	private static BufferedInputStream myStream;
	private static Font ttfBase;
	private static Font f;
	private static InputStream PATH;
	
	public Font createFont()
	{
		try {
			PATH = getClass().getClassLoader().getResourceAsStream("resources/fonts/menu_font.ttf");
            myStream = new BufferedInputStream(PATH);
            ttfBase = Font.createFont(Font.TRUETYPE_FONT, myStream);
            f = ttfBase.deriveFont(Font.PLAIN, 24);               
        } catch (Exception ex) {
            //ex.printStackTrace();
            System.err.println("Font not loaded at "+PATH+"  "+getClass().getClassLoader().getResource("resources//fonts//menu_font.ttf"));
        }
        return f;
	}
}
