
package Menu.GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

public class MenuComp extends JComponent implements MouseMotionListener, MouseListener
{
	public int stage = 0;
	public boolean quit = false;
	public int m_mode, f_mode;
	private Font MenuFont;
	private final int y1 = 252, y2 = 310, y3 = 365, y4 = 422;
	private final int z1 = 84,z2 = 140,z3 = 196,z4 = 252, z5 = 310, z6 = 365, z7 = 422;
	private final Rectangle r1 = new Rectangle(55,223,390,53), r2 = new Rectangle(55,279,390,53), r3 = new Rectangle(55,335,390,53), r4 = new Rectangle(55,391,390,53);
	private final Rectangle s1 = new Rectangle(55,111,390,53),s2 = new Rectangle(55,167,390,53), s3 = new Rectangle(55,223,390,53), s4 = new Rectangle(55,279,390,53), s5 = new Rectangle(55,335,390,53), s6 = new Rectangle(55,391,390,53);
	public MenuComp()
	{
		FontLoader f = new FontLoader();
		MenuFont = f.createFont();
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
	}
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		switch(stage)
		{
		case 0: paintBackground(g2); paintMainMenu(g2); paintMouseHover(g2); break;
		case 1: paintSelectionBackground(g2); paintStage1(g2); paintMouseHoverSelection(g2); break;
		case 2: paintSelectionBackground(g2); paintStage2(g2); paintMouseHoverSelection(g2); break;
		}
		
	}
	private void paintMouseHoverSelection(Graphics2D g2)
	{
		try{
			g2.setColor(new Color(0,150,0,100));
			if(s1.contains(this.getMousePosition()))
			{
				g2.fill(s1);
			}
			else if (s2.contains(getMousePosition()))
			{
				g2.fill(s2);
			}
			else if (s3.contains(getMousePosition()))
			{
				g2.fill(s3);
			}
			else if (s4.contains(getMousePosition()))
			{
				g2.fill(s4);
			}
			else if (s5.contains(getMousePosition()))
			{
				g2.fill(s5);
			}
			else if (s6.contains(getMousePosition()))
			{
				g2.fill(s6);
			}
			}catch(Exception ex){}
		
	}
	private void paintStage2(Graphics2D g2)
	{
		g2.setFont(MenuFont);
		g2.setColor(Color.BLACK);
		FontMetrics fm = g2.getFontMetrics();
		int x = (this.getWidth()/2) - (fm.stringWidth(System.getProperty("user.dir"))/2);
	    int y = (fm.getAscent() + (z1 - (fm.getAscent() + fm.getDescent())));
		g2.drawString(System.getProperty("user.dir"), x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Beginner")/2);
	     y = (fm.getAscent() + (z2 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Beginner", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Easy")/2);
	     y = (fm.getAscent() + (z3 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Easy", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Medium")/2);
	     y = (fm.getAscent() + (z4 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Medium", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Hard")/2);
	     y = (fm.getAscent() + (z5 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Hard", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth(" ")/2);
	     y = (fm.getAscent() + (z6 - (fm.getAscent() + fm.getDescent())));
		g2.drawString(" ", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Back")/2);
	     y = (fm.getAscent() + (z7 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Back", x, y);
		
	}
	private void paintStage1(Graphics2D g2)
	{
		g2.setFont(MenuFont);
		g2.setColor(Color.BLACK);
		FontMetrics fm = g2.getFontMetrics();
		int x = (this.getWidth()/2) - (fm.stringWidth("Select A Maze Difficulty")/2);
	    int y = (fm.getAscent() + (z1 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Select A Maze Difficulty", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Beginner")/2);
	     y = (fm.getAscent() + (z2 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Beginner", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Easy")/2);
	     y = (fm.getAscent() + (z3 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Easy", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Medium")/2);
	     y = (fm.getAscent() + (z4 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Medium", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Hard")/2);
	     y = (fm.getAscent() + (z5 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Hard", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth(" ")/2);
	     y = (fm.getAscent() + (z6 - (fm.getAscent() + fm.getDescent())));
		g2.drawString(" ", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Back")/2);
	     y = (fm.getAscent() + (z7 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Back", x, y);
		
	}
	private void paintSelectionBackground(Graphics2D g2)
	{
		try
		{
			Image b = ImageIO.read(getClass().getClassLoader().getResource("resources\\images\\SelectionBackground.jpg"));
			g2.drawImage(b,0,0,null);
		}
		catch(Exception e){}
		
	}
	public void paintBackground(Graphics2D g2)
	{
		try
		{
			Image b = ImageIO.read(getClass().getClassLoader().getResource("resources\\images\\background.jpg"));
			g2.drawImage(b,0,0,null);
		}
		catch(Exception e){}
	}
	public void paintMainMenu(Graphics2D g2)
	{
		g2.setFont(MenuFont);
		g2.setColor(Color.BLACK);
		FontMetrics fm = g2.getFontMetrics();
		int x = (this.getWidth()/2) - (fm.stringWidth("Maze Draw")/2);
	    int y = (fm.getAscent() + (y1 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Maze Draw", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Mouse Maze")/2);
	     y = (fm.getAscent() + (y2 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Mouse Maze", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Coming Soon")/2);
	     y = (fm.getAscent() + (y3 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Coming Soon", x, y);
		 x = (this.getWidth()/2) - (fm.stringWidth("Quit")/2);
	     y = (fm.getAscent() + (y4 - (fm.getAscent() + fm.getDescent())));
		g2.drawString("Quit", x, y);
	}
	public void paintMouseHover(Graphics2D g2)
	{
		try{
		g2.setColor(new Color(0,150,0,100));
		if(r1.contains(this.getMousePosition()))
		{
			g2.fill(r1);
		}
		else if (r2.contains(getMousePosition()))
		{
			g2.fill(r2);
		}
		else if (r3.contains(getMousePosition()))
		{
			g2.fill(r3);
		}
		else if (r4.contains(getMousePosition()))
		{
			g2.fill(r4);
		}
		}catch(Exception ex){}
	}
	@Override public void mouseDragged(MouseEvent e){}
	@Override public void mouseMoved(MouseEvent e){}
	@Override
	public void mouseClicked(MouseEvent arg0)
	{
		try{
			switch(stage)
			{
			case 0:
				if(r1.contains(this.getMousePosition()))
				{
					stage = 4;
				}
				else if (r2.contains(getMousePosition()))
				{
					stage = 1;
				}
				else if (r3.contains(getMousePosition()))
				{
					stage = 0;
				}
				else if (r4.contains(getMousePosition()))
				{
					quit = true;
				}
				break;
			case 1:
				if(s1.contains(this.getMousePosition()))
				{
					m_mode = 0;
					stage = 2;
				}
				else if (s2.contains(getMousePosition()))
				{
					m_mode = 1;
					stage = 2;
				}
				else if (s3.contains(getMousePosition()))
				{
					m_mode = 2;
					stage = 2;
				}
				else if (s4.contains(getMousePosition()))
				{
					m_mode = 3;
					stage = 2;
				}
				else if (s5.contains(getMousePosition()))
				{
					
				}
				else if (s6.contains(getMousePosition()))
				{
					stage = 0;
				}
				break;
			case 2:
				if(s1.contains(this.getMousePosition()))
				{
					f_mode = 0;
					stage = 3;
				}
				else if (s2.contains(getMousePosition()))
				{
					f_mode = 1;
					stage = 3;
				}
				else if (s3.contains(getMousePosition()))
				{
					f_mode = 2;
					stage = 3;
				}
				else if (s4.contains(getMousePosition()))
				{
					f_mode = 3;
					stage = 3;
				}
				else if (s5.contains(getMousePosition()))
				{
					
				}
				else if (s6.contains(getMousePosition()))
				{
					stage = 1;
				}
				break;
			}
			
			}catch(Exception ex){}
	}
	@Override public void mouseEntered(MouseEvent arg0){}
	@Override public void mouseExited(MouseEvent arg0){}
	@Override public void mousePressed(MouseEvent arg0){}
	@Override public void mouseReleased(MouseEvent arg0){}
}
