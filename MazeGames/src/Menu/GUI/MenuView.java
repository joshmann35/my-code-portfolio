
package Menu.GUI;

import gridSolverMaze.GUI.GridMazeView;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;
import javax.swing.Timer;
import mouseMaze.GUI.MouseMazeView;

public class MenuView
{
	private JFrame frame;
	private MenuComp panel;
	private GridMazeView v = null;
	private MouseMazeView m = null;
	private boolean inGame = false;
	private Timer timer = new Timer(1 , new ActionListener(){
    	public void actionPerformed(ActionEvent e) {
    		if(!inGame)
    		{
    			if(panel.quit)
        		{
        			System.exit(0);
        		}
        		if(panel.stage==4)
        		{
        			frame.getContentPane().remove(panel);
        			v = new GridMazeView(frame);
        			inGame = true;
        		}
        		if(panel.stage==3)
        		{
        			frame.getContentPane().remove(panel);
        			m = new MouseMazeView(frame, panel.m_mode, panel.f_mode);
        			inGame = true;
        		}
        		panel.repaint();
    		}
    		else if(v!=null&&v.getIsEnd())
    		{
    			v = null;
    			init();
    			inGame = false;
    		}
    		else if(m!=null&&m.getIsEnd())
    		{
    			m = null;
    			init();
    			inGame = false;
    		}	
  	  }
    });
	public MenuView()
	{
		frame = new JFrame("Joe's Maze Games");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		init();
	}
	public void init()
	{
		panel = new MenuComp();
		panel.setPreferredSize(new Dimension(500, 500));
		frame.setSize(panel.getPreferredSize());
		frame.getContentPane().add(panel);
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		timer.start();
	}
}
