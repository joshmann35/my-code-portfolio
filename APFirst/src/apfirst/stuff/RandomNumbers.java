package apfirst.stuff;

import java.util.Random;


public class RandomNumbers
{

	public static void main(String[] args)
	{
		
		Random g = new Random();
		int num1;
		double num2;
		num1=g.nextInt(10);
		System.out.println("from 0 to 9: "+num1);
		num1=g.nextInt(10)+1;
		System.out.println("1-10: "+num1);
		num1=g.nextInt(15)+20;
		System.out.println("20-34: "+num1);
		num1=g.nextInt(20)-10;
		System.out.println("-10to9: "+num1);
		num2=g.nextDouble();
		System.out.println("double 0 to 1: "+num2);
		num2=g.nextDouble()*6;
		num1=(int)num2+1;
		System.out.println("1-6: "+num1);
		// I LIKE PIE!
	}

}
