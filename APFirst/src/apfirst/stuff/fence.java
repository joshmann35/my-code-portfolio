
package apfirst.stuff;

import java.util.Scanner;

public class fence
{
	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		
		Scanner in = new Scanner(System.in);
		System.out.print("Enter Width: ");
		double width = in.nextDouble();
		System.out.print("Enter Height: ");
		double height = in.nextDouble();
		System.out.print("Enter Price of Fence: ");
		double fence = in.nextDouble();
		int cost = (int)(((width+height)*2)*fence*100);
		String costs = String.valueOf(cost/100)+"."+String.valueOf(cost%100);
		System.out.println("It would cost $"+costs+" to fence a "+width+" foot by "+height+" foot area");
		
	}
}
