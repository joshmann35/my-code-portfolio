
package apfirst.stuff;

public class IceCream
{
	enum Flavor {vanilla, chocolate, strawberry, fudgeRipple, rockyRoad, mintChocolateChip, cookieDough};
	public static void main(String[] args)
	{
		Flavor cone1, cone2, cone3;
		cone1 = Flavor.vanilla;
		cone2 = Flavor.mintChocolateChip;
		System.out.println("Cone 1: "+cone1);
		System.out.println("Cone 2: "+cone2);
		cone3 = cone1;
		System.out.println("Cone 3: "+cone3);
	}
}
