
package apfirst.stuff;

import java.util.Scanner;

public class SecondsToMinutes
{
	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		System.out.print("Enter the amount of seconds: ");
		int sec = in.nextInt();
		if(sec/60>=60){
			if((sec/60)/60>=24){
				if(((sec/60)/60)/24>=30){
					if((((sec/60)/60)/24)>=365){
						System.out.println(sec+" seconds is "+((((sec/60)/60)/24)/365)+" years and "+((((sec/60)/60)/24)/30)%12+" months and "+(((sec/60)/60)/24)%30+" days and "+((sec/60)/60)%24+" hours and "+((sec/60)%60)+" minutes and "+(sec%60)+" seconds");
					}else{
						System.out.println(sec+" seconds is "+((((sec/60)/60)/24)/30)+" months and "+(((sec/60)/60)/24)%30+" days "+((sec/60)/60)%24+" hours and "+((sec/60)%60)+" minutes and "+(sec%60)+" seconds");
					}
				}else{
					System.out.println(sec+" seconds is "+(((sec/60)/60)/24)+" days "+((sec/60)/60)+" hours and "+(sec/60)+" minutes and "+(sec%60)+" seconds");
				}
			}else{
				System.out.println(sec+" seconds is "+((sec/60)/60)+" hours and "+(sec/60)+" minutes and "+(sec%60)+" seconds");
			}
		}else{
			System.out.println(sec+" seconds is "+(sec/60)+" minutes and "+(sec%60)+" seconds");
		}
	}
}
