package Activity1;
/**
 * This is a class that tests the Card class.
 */
public class CardTester {

	/**
	 * The main method in this class checks the Card operations for consistency.
	 *	@param args is not used.
	 */
	public static void main(String[] args) {
		/* *** TO BE IMPLEMENTED IN ACTIVITY 1 *** */
		Card a = new Card("Ace", "Spades", 1);
		Card b = new Card("Two", "Hearts", 2);
		Card c = new Card("Three", "Clubs", 3);
		Card d = new Card("King", "Diamonds", 13);
		System.out.println(a.pointValue());
		System.out.println(b.pointValue());
		System.out.println(c.pointValue());
		System.out.println(d.pointValue());
		System.out.println(a.rank());
		System.out.println(b.rank());
		System.out.println(c.rank());
		System.out.println(d.rank());
		System.out.println(a.suit());
		System.out.println(b.suit());
		System.out.println(c.suit());
		System.out.println(d.suit());
		System.out.println(a.matches(a));
		System.out.println(b.matches(a));
		System.out.println(c.matches(a));
		System.out.println(d.matches(a));
		System.out.println(a.matches(b));
		System.out.println(b.matches(b));
		System.out.println(c.matches(b));
		System.out.println(d.matches(b));
		System.out.println(a.matches(c));
		System.out.println(b.matches(c));
		System.out.println(c.matches(c));
		System.out.println(d.matches(c));
		System.out.println(a.matches(d));
		System.out.println(b.matches(d));
		System.out.println(c.matches(d));
		System.out.println(d.matches(d));
		
	}
}
