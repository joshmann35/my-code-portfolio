package Activity2;
/**
 * This is a class that tests the Deck class.
 */
public class DeckTester {

	/**
	 * The main method in this class checks the Deck operations for consistency.
	 *	@param args is not used.
	 */
	public static void main(String[] args) {
		String[] a1 = {"One","Two"};
		String[] b1 = {"Purple", "Orange"};
		int[] c1 = {5, 2};
		Deck a = new Deck(a1, b1, c1);
		System.out.println(a.deal());
		System.out.println(a.isEmpty());
		System.out.println(a.size());
		System.out.println(a.deal());
		System.out.println(a.size());
		System.out.println(a.deal());
		System.out.println(a.size());
		System.out.println(a.deal());
		System.out.println(a.size());
		System.out.println(a.deal());
		System.out.println(a.size());
		System.out.println(a.deal());
		System.out.println(a.isEmpty());

		System.out.println(a.size());
		System.out.println(a.deal());
		System.out.println(a.size());
		System.out.println(a.deal());
	}
}


/*
 * 
 * 
 * 1) A deck holds a bunch of card objects
 * 
 *  2) 6
 *  
 *  3) String[] ranks = {"Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"};
 *     String[] suits = {"Diamond", "Heart", "Club", "Spade"};
 *     int[] pointValues = {2, 3, 4, 5, 6, 7, 8 ,9 ,10, 10, 10 ,10, 11};
 *     
 *  4) YES, they are passed into the Constructor in that order, so if you mixed em up you done messed up !
 * 	   They are paralell
 * 
 */
