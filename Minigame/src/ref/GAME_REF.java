package ref;

import java.awt.Color;

public class GAME_REF
{
	
	// GAME CYCLE TIMER
	public static final int GAME_CYCLE_TIME = 40;

	// FRAME
	public static final int BORDER_X = 500;
	public static final int BORDER_Y = 500;

	// PLAYER
	public static final int PLAYER_START_X = 0;
	public static final int PLAYER_START_Y = 0;
	public static final int PLAYER_WIDTH = 20;
	public static final int PLAYER_HEIGHT = 20;
	public static final int PLAYER_HEALTH = 100;
	public static final int MAX_SHOTS = 100;
	public static final int MAX_PLAYER_POWS = 2;
	public static final int START_KILLS = 0;
	public static final int START_SHOTS = 0;
	public static final int START_NUM_OF_POWS = 0;
	public static final int PLAYER_MOVE_MOD = 2;

	// ENEMY
	public static final int ENEMY_START_X = 250;
	public static final int ENEMY_START_Y = 250;
	public static final int ENEMY_WIDTH = 20;
	public static final int ENEMY_HEIGHT = 20;
	public static final int ENEMY_HEALTH = 100;
	public static final int ENEMY_SPEED_RANGE = 10;
	public static final int ENEMY_DAMAGE = 2;
	public static final int ENEMY_HEALTH_BAR_MOD = ENEMY_HEALTH/ENEMY_WIDTH;

	// KEYS
	public static final int KEY_FORWARD = 87;
	public static final int KEY_LEFT = 65;
	public static final int KEY_REVERSE = 83;
	public static final int KEY_RIGHT = 68;
	public static final int KEY_SHOOT = 32;
	public static final int KEY_USE_POW = 16;
	public static final int KEY_CHEAT_MODE = 192;

	// POWER UPS
	public static final int MAX_POWS = 1; // MAX POWERUPS ON SCREEN
	public static final int POW_WIDTH = 10;
	public static final int POW_HEIGHT = 10;

	// POWER UP TIMERS
	public static final int SHIELD_TIME = 5000;
	public static final int INF_SHOT_TIME = 10000;
	public static final int MULTI_SHOT_TIME = 10000;

	// BULLET
	public static final int BULLET_DAMAGE = 10;
	public static final int BULLET_WIDTH = 6;
	public static final int BULLET_HEIGHT = 2;
	public static final double BULLET_SPEED = 20.0;
	
	// COLORS
	public static final Color DEFAULT_COLOR = Color.BLACK;
	public static final Color PLAYER_HEALTH_BAR_COLOR = Color.GREEN;
	public static final Color PLAYER_SHOT_COOLDOWN_COLOR = Color.PINK;
	public static final Color SHEILD_COLOR = Color.CYAN;
	public static final Color INF_SHOT_COLOR = Color.ORANGE;
	public static final Color MULTI_SHOT_COLOR = new Color(0,128,0);
	public static final Color ENEMY_COLOR = Color.MAGENTA;
	public static final Color ENEMY_HEALTH_BAR_COLOR = Color.RED;
	public static final Color BACKGROUND_COLOR = Color.WHITE;
	public static final Color DEFAULT_BULLET_COLOR = Color.RED;
	public static final Color PLAYER_COLOR = Color.BLUE;
	
	
	// TEXTURES
	public static final String PLAYER_TEXTURE_URL= "/textures/player/player.png";
	
}
