package minigame;

import java.awt.Rectangle;

import ref.GAME_REF;

public class Unit {
	private int x;
	private int y;
	private int w;
	private int h;
	private int health;
	private int borderX;
	private int borderY;
	private Rectangle r;
	Unit(int x, int y, int width, int height, int health, int borderX, int borderY)
	{
		r = new Rectangle();
		r.setRect(x, y, width, height);
		this.x=x;
		this.y=y;
		this.w=width;
		this.h=height;
		this.health=health;
		this.borderX=borderX;
		this.borderY=borderY;
	}
	
	public boolean intersects(Rectangle a)
	{
		return r.intersects(a);
	}
	
	public void move(int d)
	{
		switch(d)
		{
		case 0: this.x-=(this.w/GAME_REF.PLAYER_MOVE_MOD); this.r.x=this.x; break;
		case 1: this.y-=(this.h/GAME_REF.PLAYER_MOVE_MOD); this.r.y=this.y; break;
		case 2: this.x+=(this.w/GAME_REF.PLAYER_MOVE_MOD); this.r.x=this.x; break;
		case 3: this.y+=(this.h/GAME_REF.PLAYER_MOVE_MOD); this.r.y=this.y; break;
		}
		checkOverShoot();
	}
	
	public void checkOverShoot()
	{
		int x = this.getX();
		int y = this.getY();
		int w = this.getW();
		int h = this.getH();
		int overshoot = (int)((x+w)-this.borderX);
		if(x<=0)
			x=0;
		if(overshoot>=0)
			x=(this.borderX-w);
		overshoot = (int)((y+h)-this.borderY);
		if(y<=0)
			y=0;
		if(overshoot>=0)
			y=(this.borderY-h);
		this.setX(x);
		this.setY(y);
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
		this.r.x=x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
		this.r.y=y;
	}
	public int getW() {
		return w;
	}
	public void setW(int w) {
		this.w = w;
		this.r.width=w;
	}
	public int getH() {
		return h;
	}
	public void setH(int h) {
		this.h = h;
		this.r.height=h;
	}
	
	public Rectangle getRect(){
		return r.getBounds();
	}
	public int getHealth()
	{
		return this.health;
	}
	public void setHealth(int newHealth)
	{
		this.health=newHealth;
	}
	public void takeDamage(int damage)
	{
		this.health-=damage;
	}
	
}
