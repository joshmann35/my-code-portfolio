package minigame;

import java.awt.Rectangle;

import ref.GAME_REF;

public class Bullet{
	private int x;
	private int y;
	private int xSpeed;
	private int ySpeed;
	private int degrees;
	private int height;
	private int width;
	private Rectangle r;
	private int damage;
	public Bullet(int x, int y, int xSpeed, int ySpeed, int degrees, int damage)
	{
		this.x=x;
		this.y=y;
		this.xSpeed=xSpeed;
		this.ySpeed=ySpeed;
		height = GAME_REF.BULLET_HEIGHT;
		width = GAME_REF.BULLET_WIDTH;
		this.degrees=degrees;
		r = new Rectangle();
		r.setRect(x, y, width, height);
		this.damage = damage;
	}
	
	public void move(double direction)
	{
		
		double speed = Math.sqrt(xSpeed*xSpeed + ySpeed*ySpeed);
	    speed = Math.min(speed, GAME_REF.BULLET_SPEED);
	    int x_inc = (int)(speed * Math.cos(direction));
	    int y_inc = (int)(speed * Math.sin(direction));
	    this.setX(this.x+x_inc);
		this.setY(this.y+y_inc);
	}
	public void cheatMove()
	{
		int x_inc = (int)(Math.cos(degrees));
	    int y_inc = (int)(Math.sin(degrees));
	    this.setX(this.x+x_inc);
		this.setY(this.y+y_inc);
	}
	public boolean intersects(Rectangle o)
	{
		return r.intersects(o);
	}
	public int getW()
	{
		return this.width;
	}
	public int getH()
	{
		return this.height;
	}
	public int getX()
	{
		return this.x;
	}
	public int getY()
	{
		return this.y;
	}
	public void setX(int x)
	{
		this.x=x;
		this.r.x=x;
	}
	public void setY(int y)
	{
		this.y=y;
		this.r.y=y;
	}
	public Rectangle getRect()
	{
		return this.r.getBounds();
	}
	public int getxSpeed() {
		return xSpeed;
	}

	public void setxSpeed(int xSpeed) {
		this.xSpeed = xSpeed;
	}

	public int getySpeed() {
		return ySpeed;
	}

	public void setySpeed(int ySpeed) {
		this.ySpeed = ySpeed;
	}
	public int getDamage()
	{
		return this.damage;
	}
	public void setDamage(int damage)
	{
		this.damage=damage;
	}
}
