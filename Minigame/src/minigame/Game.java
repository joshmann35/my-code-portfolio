package minigame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.Timer;

import ref.GAME_REF;

public class Game extends JFrame implements KeyListener{
	private List <Player> one;
	private List <Enemy> two;
	private List <Bullet> b;
	private List <Powerup> p;
	private boolean[] keys;
	private boolean isPlaying, usingShield, multiShot, ionShot, usingPower, cheatMode;
	private GameView panel;
	private JFrame frame;
	private int borderX, borderY;
	//Scanner in = new Scanner(System.in);
    private Timer timer = new Timer(GAME_REF.GAME_CYCLE_TIME , new ActionListener(){
    	public void actionPerformed(ActionEvent e) {
    		if(isPlaying)
    		{
    			updateMove();
    			checkIntersect();
    		}
  	  }
    });
    
	@SuppressWarnings("deprecation")
	Game()
	{
		//System.out.println("Enter start (0-500): ");
		//int strt = in.nextInt();
		borderX = GAME_REF.BORDER_X;
		borderY = GAME_REF.BORDER_Y;
		b = new ArrayList<Bullet>();
		one = new ArrayList<Player>();
		two = new ArrayList<Enemy>();
		p = new ArrayList<Powerup>();
		usingShield=false;
		usingPower=false;
		ionShot=false;
		multiShot=false;
		cheatMode = false;
		keys = new boolean[5];
		one.add(new Player(GAME_REF.PLAYER_START_X,GAME_REF.PLAYER_START_Y,GAME_REF.PLAYER_WIDTH,GAME_REF.PLAYER_HEIGHT,GAME_REF.PLAYER_HEALTH,borderX,borderY));
		two.add(new Enemy(GAME_REF.ENEMY_START_X,GAME_REF.ENEMY_START_Y,GAME_REF.ENEMY_WIDTH,GAME_REF.ENEMY_HEIGHT,GAME_REF.ENEMY_HEALTH,borderX,borderY));
		panel = new GameView(one,b,two,p);
		isPlaying = true;
		//System.out.println("Starting Game...");
		frame = new JFrame("Game");
		frame.setSize(borderX+17,borderY+40);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addKeyListener(this);
		frame.setFocusable(true);
		frame.requestFocusInWindow();
		frame.setVisible(true);
		frame.getContentPane().add(panel);
		frame.setCursor(JFrame.CROSSHAIR_CURSOR);
		//System.out.println(panel.getWidth()+" "+panel.getHeight());
		startTimer();
	}
	
	@Override
	public void keyPressed(KeyEvent e)
	{
		//System.out.println(e.getKeyCode());
		switch(e.getKeyCode())
		{
		case GAME_REF.KEY_FORWARD: keys[1]=true;break;
		case GAME_REF.KEY_LEFT: keys[0]=true;break;
		case GAME_REF.KEY_REVERSE: keys[3]=true;break;
		case GAME_REF.KEY_RIGHT: keys[2]=true;break;
		case GAME_REF.KEY_SHOOT: keys[4]=true;break;
		case GAME_REF.KEY_USE_POW: if(one.get(0).getNumOfPows()>0&&!usingPower)usePowerup(); break;
		case GAME_REF.KEY_CHEAT_MODE: cheatMode = true; panel.setCheatMode(true); break;
		}
		//System.out.println(e.getKeyChar());
	}

	
	public void updateMove()
	{
		//System.out.println(keys);
		
		for(int i = 0; i < keys.length; i++)
		{
			if(keys[i])
			{
			switch(i)
			{
			case 1: one.get(0).move(1); break;
			case 0: one.get(0).move(0); break;
			case 3: one.get(0).move(3); break;
			case 2: one.get(0).move(2); break;
			case 4: if(one.get(0).getShots()<GAME_REF.MAX_SHOTS){
				try
				{
					if(cheatMode)
					{
						for(int j = 0; j<=360; j++)
						{
							b.add(new Bullet((one.get(0).getX()+one.get(0).getW()/2),(one.get(0).getY()+one.get(0).getH()/2), 1, 1, j, 100));
						}
					}
					else if(multiShot)
					{
//						b.add(new Bullet((one.get(0).getX()+one.get(0).getW()/2),(one.get(0).getY()+one.get(0).getH()/2), ((int)(panel.getMousePosition().getX()-(one.get(0).getX()+one.get(0).getW()/2))), ((int)(panel.getMousePosition().getY()-(one.get(0).getY()+one.get(0).getH()/2))), 1, 1, 10));
//						b.add(new Bullet((one.get(0).getX()+one.get(0).getW()/2),(one.get(0).getY()+one.get(0).getH()/2), ((int)(panel.getMousePosition().getX()-(one.get(0).getX()+one.get(0).getW()/2))-10), ((int)(panel.getMousePosition().getY()-(one.get(0).getY()+one.get(0).getH()/2))-10), 1, 1, 10));
//						b.add(new Bullet((one.get(0).getX()+one.get(0).getW()/2),(one.get(0).getY()+one.get(0).getH()/2), ((int)(panel.getMousePosition().getX()-(one.get(0).getX()+one.get(0).getW()/2))+10), ((int)(panel.getMousePosition().getY()-(one.get(0).getY()+one.get(0).getH()/2))+10), 1, 1, 10));
						b.add(new Bullet((one.get(0).getX()+one.get(0).getW()/2),(one.get(0).getY()+one.get(0).getH()/2), ((int)(panel.getMousePosition().getX()-(one.get(0).getX()+one.get(0).getW()/2))), ((int)(panel.getMousePosition().getY()-(one.get(0).getY()+one.get(0).getH()/2))), 1, GAME_REF.BULLET_DAMAGE));
						b.add(new Bullet((one.get(0).getX()),(one.get(0).getY()), ((int)(panel.getMousePosition().getX()-(one.get(0).getX()))), ((int)(panel.getMousePosition().getY()-(one.get(0).getY()))-10), 1, GAME_REF.BULLET_DAMAGE));
						b.add(new Bullet((one.get(0).getX()+one.get(0).getW()),(one.get(0).getY()+one.get(0).getH()), ((int)(panel.getMousePosition().getX()-(one.get(0).getX()+one.get(0).getW()))), ((int)(panel.getMousePosition().getY()-(one.get(0).getY()+one.get(0).getH()))+10), 1, GAME_REF.BULLET_DAMAGE));
						//System.out.println("UBER Pew"); 
					}
					else if(ionShot)
					{
						b.add(new Bullet((one.get(0).getX()+one.get(0).getW()/2),(one.get(0).getY()+one.get(0).getH()/2), ((int)(panel.getMousePosition().getX()-(one.get(0).getX()+one.get(0).getW()/2))), ((int)(panel.getMousePosition().getY()-(one.get(0).getY()+one.get(0).getH()/2))), 1, GAME_REF.BULLET_DAMAGE)); 
						//System.out.println("EXTRA Pew");
					}
					else{
						b.add(new Bullet((one.get(0).getX()+one.get(0).getW()/2),(one.get(0).getY()+one.get(0).getH()/2), ((int)(panel.getMousePosition().getX()-(one.get(0).getX()+one.get(0).getW()/2))), ((int)(panel.getMousePosition().getY()-(one.get(0).getY()+one.get(0).getH()/2))), 1, GAME_REF.BULLET_DAMAGE)); 
						//System.out.println("Pew"); 
					}
					
				}
				catch(NullPointerException npe){}
			}break;}
			}
			if(keys[4]==true && one.get(0).getShots()<=GAME_REF.MAX_SHOTS)
			{
				if(!ionShot)
					if(!cheatMode)
						one.get(0).incShots();
			}
			else if(one.get(0).getShots()>=0)
			{
				one.get(0).decShots();
			}
			
		}
		if(!two.isEmpty()&&!usingShield)
			for(int i = 0; i < two.size(); i++)
				two.get(i).move(one,(Math.random()*GAME_REF.ENEMY_SPEED_RANGE)+(one.get(0).getKills()/3));
		
		if(one.get(0).getKills()>0&&p.size()<GAME_REF.MAX_POWS&&one.get(0).getNumOfPows()<GAME_REF.MAX_PLAYER_POWS&&!usingPower)
		{
			p.add(new Powerup((int)(Math.random()*(borderX-GAME_REF.POW_WIDTH)),(int)(Math.random()*(borderY-GAME_REF.POW_HEIGHT)), GAME_REF.POW_WIDTH, GAME_REF.POW_HEIGHT, (int)(Math.random()*3)));
		}
		
		if(two.get(0).getHealth()<=0)
		{
			two.remove(0);
			two.add(new Enemy((int)(Math.random()*borderX),(int)(Math.random()*borderY),GAME_REF.ENEMY_WIDTH,GAME_REF.ENEMY_HEIGHT,GAME_REF.ENEMY_HEALTH, borderX, borderY));
			one.get(0).addEnemyKilled();
		}
		if(one.get(0).getHealth()<= 0)
		{
			isPlaying = false;
			panel.setPlaying(false);
			panel.repaint();
		}
		
		panel.repaint();
	}
	
	public void startTimer()
	{
        timer.start();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		switch(e.getKeyCode())
		{
		case GAME_REF.KEY_FORWARD: keys[1]=false;break;
		case GAME_REF.KEY_LEFT: keys[0]=false;break;
		case GAME_REF.KEY_REVERSE: keys[3]=false;break;
		case GAME_REF.KEY_RIGHT: keys[2]=false;break;
		case GAME_REF.KEY_SHOOT: keys[4]=false;break;
		}
		
	}

	@Override public void keyTyped(KeyEvent e) {}
	
	
	public void checkIntersect()
	{
		for(int i = 0; i < b.size(); i++)
		{
			if(b.get(i).intersects(two.get(0).getRect()))
			{
				two.get(0).takeDamage(b.get(i).getDamage());
				b.remove(i);
			}
		}
		if(one.get(0).intersects(two.get(0).getRect()))
		{
			if(!cheatMode)
				one.get(0).takeDamage(GAME_REF.ENEMY_DAMAGE);
		}
		if(!p.isEmpty())
		{
			if(one.get(0).intersects(p.get(0).getRect()))
			{
				one.get(0).addPowerup(p.get(0));
				p.remove(0);
			}
		}
		
	}
	
	public void usePowerup()
	{
		panel.usePowerup(one.get(0).getPList().get(0).getType());
		switch(one.get(0).getPList().get(0).getType())
		{
		case 0: 
			usingShield=true;
			usingPower =true;
			Timer ptime = new Timer(GAME_REF.SHIELD_TIME , new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					usingShield=false;
					usingPower =false;
					panel.disablePowerup();
				}
			});
			ptime.setRepeats(false);
			ptime.start();
		break;
		case 1: 
			ionShot=true;
			usingPower =true;
			Timer itime = new Timer(GAME_REF.INF_SHOT_TIME , new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					ionShot=false;
					usingPower =false;
					panel.disablePowerup();
				}
			});
			itime.setRepeats(false);
			itime.start();
			break;
		case 2: 
			multiShot=true;
			usingPower =true;
			Timer mtime = new Timer(GAME_REF.MULTI_SHOT_TIME , new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					multiShot=false;
					usingPower =false;
					panel.disablePowerup();
				}
			});
			mtime.setRepeats(false);
			mtime.start();
			break;
		}
		one.get(0).usePowerup();
		
	}
	
	
	
	
}
