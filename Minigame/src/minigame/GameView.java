package minigame;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;

import ref.GAME_REF;

public class GameView extends JComponent
{
	private List <Player> one;
	private List <Enemy> two;
	private List <Bullet> b;
	private List <Powerup> p;
	private double bangle;
	private boolean isPlaying, usingPowerup, doneAddingLeaderStat, cheatMode;
	private JFormattedTextField name;
	private JButton submit;
	private int powerType;
	private Color bulletColor;
	GameView(List <Player> one, List <Bullet> b, List <Enemy> two, List<Powerup> p)
	{
		this.one=one;
		this.b=b;
		this.two=two;
		this.p=p;
		isPlaying = true;
		usingPowerup = false;
		doneAddingLeaderStat = false;
		cheatMode = false;
		bulletColor = GAME_REF.DEFAULT_BULLET_COLOR;
	}
	public void setPlaying(boolean isPlaying)
	{
		this.isPlaying = isPlaying;
	}
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		FontMetrics fm = g2.getFontMetrics();
		// EXECUTE EVERY LIVING CYCLE
		if(isPlaying){
		double angle = 0;
		double eangle = 0;
		bangle = 0;
		if(!b.isEmpty())
		{
			try{
				for(int i = 0; i < b.size(); i++){
					if(b.get(i).getX()>GAME_REF.BORDER_X||b.get(i).getY()>GAME_REF.BORDER_Y||b.get(i).getX()<0||b.get(i).getY()<0)
					{
						b.remove(i);
					}
					else{
						bangle = Math.atan2(b.get(i).getySpeed(), b.get(i).getxSpeed());
						AffineTransform transbul = new AffineTransform();
						transbul.rotate(bangle, b.get(i).getX()+ b.get(i).getW()/2, b.get(i).getY()+b.get(i).getH()/2);
						Shape bt = transbul.createTransformedShape(b.get(i).getRect());
						g2.setColor(bulletColor);
						g2.fill(bt);
						b.get(i).move(bangle);
					}
				}
			}catch(NullPointerException npess){}
		}
		if(!one.isEmpty())
		{
		try{
			angle = Math.atan2((this.getMousePosition().getY()-(one.get(0).getY()+one.get(0).getH()/2)),(this.getMousePosition().getX()-(one.get(0).getX()+one.get(0).getW()/2)));
		}catch(NullPointerException e){}
		AffineTransform transform = new AffineTransform();
		transform.rotate(angle, one.get(0).getX() + one.get(0).getW()/2, one.get(0).getY() + one.get(0).getH()/2);
		Shape t = transform.createTransformedShape(one.get(0).getRect());
		g2.setColor(GAME_REF.PLAYER_COLOR);
		//g2.fill(t);
		g2.setTransform(transform);
		Image img = null;
		try
		{
			img = ImageIO.read(new File(System.getProperty("user.dir")+GAME_REF.PLAYER_TEXTURE_URL));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		g2.drawImage(img, one.get(0).getX(), one.get(0).getY(), null);
		g2.setTransform(new AffineTransform());
		g2.setColor(GAME_REF.DEFAULT_COLOR);
		g2.drawString("Health: ", 10, 20);
		g2.drawString("Kills: "+one.get(0).getKills(), 275, 20);
		g2.setColor(GAME_REF.DEFAULT_COLOR);
		g2.fillRect(55, 10, GAME_REF.PLAYER_HEALTH, 10);
		g2.setColor(GAME_REF.PLAYER_HEALTH_BAR_COLOR);
		g2.fillRect(55, 10, one.get(0).getHealth(), 10);
		g2.setColor(GAME_REF.PLAYER_SHOT_COOLDOWN_COLOR);
		g2.fillRect(350,10,one.get(0).getShots(), 10);
		if(cheatMode)
		{
			g2.setColor(GAME_REF.DEFAULT_BULLET_COLOR);
			g2.drawString("CHEAT", 10, 45);
		}
		try{
			switch(one.get(0).getPList().get(0).getType())
			{
			case 0: g2.setColor(GAME_REF.SHEILD_COLOR); break;
			case 1: g2.setColor(GAME_REF.INF_SHOT_COLOR); break;
			case 2: g2.setColor(GAME_REF.MULTI_SHOT_COLOR); break;
			}
		}catch(IndexOutOfBoundsException npee){}
		g2.drawString("Pows: "+one.get(0).getNumOfPows(), 175, 20);
		}
		
		if(!two.isEmpty())
		{
			eangle = Math.atan2((one.get(0).getY()-(two.get(0).getY()+two.get(0).getH()/2)),(one.get(0).getX()-(two.get(0).getX()+two.get(0).getW()/2)));
			AffineTransform etransform = new AffineTransform();
			etransform.rotate(eangle, two.get(0).getX() + two.get(0).getW()/2, two.get(0).getY() + two.get(0).getH()/2);
			Shape e = etransform.createTransformedShape(two.get(0).getRect());
			Rectangle health = new Rectangle(two.get(0).getX(), two.get(0).getY()-10, two.get(0).getHealth()/(GAME_REF.ENEMY_HEALTH_BAR_MOD), 4);
			Rectangle healthBox = new Rectangle(two.get(0).getX(), two.get(0).getY()-10, two.get(0).getW(), 4);
			
			g2.setColor(GAME_REF.ENEMY_COLOR);
			g2.fill(e);
			g2.setColor(GAME_REF.ENEMY_HEALTH_BAR_COLOR);
			g2.fill(health);
			g2.setColor(GAME_REF.DEFAULT_COLOR);
			g2.draw(healthBox);
		}
		
		if(usingPowerup)
		{
			switch(powerType)
			{
			case 0: g2.setColor(GAME_REF.SHEILD_COLOR); g2.drawOval(one.get(0).getX()-10, one.get(0).getY()-10, 40, 40); break;
			case 1: bulletColor = GAME_REF.INF_SHOT_COLOR;break;
			case 2: bulletColor = GAME_REF.MULTI_SHOT_COLOR; break;
			}
			
		}
		
		if(!p.isEmpty())
		{
			switch(p.get(0).getType())
			{
			case 0: g2.setColor(GAME_REF.SHEILD_COLOR);
				g2.fillRect(p.get(0).getX(), p.get(0).getY(), p.get(0).getW(), p.get(0).getH()); break;
			case 1: g2.setColor(GAME_REF.INF_SHOT_COLOR);
				g2.fillRect(p.get(0).getX(), p.get(0).getY(), p.get(0).getW(), p.get(0).getH()); break;
			case 2: g2.setColor(GAME_REF.MULTI_SHOT_COLOR);
				g2.fillRect(p.get(0).getX(), p.get(0).getY(), p.get(0).getW(), p.get(0).getH()); break;
			}
			
		}
		
		
		} // END OF ISPLAYING
		
		else // EXECUTE WHEN PLAYER DIES
		{
			
			g2.setColor(GAME_REF.BACKGROUND_COLOR);
			g2.drawRect(0, 0, this.getWidth(), this.getHeight());
			g2.setColor(GAME_REF.DEFAULT_COLOR);
			// PROMPT AND ADD SCORE TO LEADERBOARD
			if(!doneAddingLeaderStat&&!cheatMode)
			{
			name = new JFormattedTextField();
			this.add(name);
			name.setBounds(100, 50, 200, 50);
			name.setVisible(true);
//			name.getDocument().addDocumentListener(new DocumentListener(){
//				@Override
//				public void changedUpdate(DocumentEvent arg0)
//				{
//					name.setText(name.getText().toUpperCase());
//				}
//				@Override
//				public void insertUpdate(DocumentEvent arg0){
//					name.setText(name.getText().toUpperCase());}
//				@Override
//				public void removeUpdate(DocumentEvent arg0){
//					name.setText(name.getText().toUpperCase());}
//			});
			submit = new JButton("Submit(ONLY 3 LETTERS)");
			this.add(submit);
			submit.setBounds(100, 100, 200, 50);
			submit.setVisible(true);
			submit.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent arg0)
				{
					addLeaderStat();
					name.setVisible(false);
					submit.setVisible(false);
					doneAddingLeaderStat=true;
					repaintStuff();
				}
			});
			
			}
			
			// DISPLAY LEADERBOARD
			if(doneAddingLeaderStat)
			{
				List<String[]> dm = getLeaderboard();
				int tempPlace = 0;
				int stringX = 10;
				int stringY = 28;
				while(!dm.isEmpty())
				{
					for(int i = 0; i < dm.size();i++)
					{
						int dmTemp = Integer.parseInt(dm.get(i)[1]);
						int dmCurr = Integer.parseInt(dm.get(tempPlace)[1]);
						if(dmTemp>dmCurr)
						{
							tempPlace=i;
						}
					}
					g2.drawString(dm.get(tempPlace)[0]+" --- "+dm.get(tempPlace)[1], stringX, stringY);
					stringY+=(fm.getHeight()+8);
					dm.remove(tempPlace);
					tempPlace=0;
				}
				g2.drawString("LEADERBOARD", 0, 10);
			}
		}
		
		
	}
	
	public void usePowerup(int type)
	{
		usingPowerup=true;
		powerType = type;
	}
	public void disablePowerup()
	{
		usingPowerup=false;
		bulletColor = GAME_REF.DEFAULT_BULLET_COLOR;
	}
	public List getLeaderboard()
	{
		File leadStats = new File("leaderStats.txt");
		List<String[]> stats = new ArrayList<String[]>();
		try
		{
			Scanner in = new Scanner(leadStats);
			while(in.hasNext())
			{
				String[] temp = new String[2];
				temp[0]=(in.next());
				//System.out.println(temp[0]);
				temp[1]=(in.next());
				//System.out.println(temp[1]);
				stats.add(temp);
			}
		} catch (FileNotFoundException e){}
		return stats;
	}
	public void addLeaderStat()
	{
		FileWriter fw;
		try
		{
			fw = new FileWriter("leaderStats.txt",true);
			switch(name.getText().length())
			{
			case 0: fw.write("NaN"+" "+one.get(0).getKills()+"\n"); break;
			case 1: fw.write(String.valueOf(name.getText().charAt(0)).toUpperCase()+"_"+"_"+" "+one.get(0).getKills()+"\n"); break;
			case 2: fw.write(String.valueOf(name.getText().charAt(0)).toUpperCase()+String.valueOf(name.getText().charAt(1)).toUpperCase()+"_"+" "+one.get(0).getKills()+"\n"); break;
			default: fw.write(String.valueOf(name.getText().charAt(0)).toUpperCase()+String.valueOf(name.getText().charAt(1)).toUpperCase()+String.valueOf(name.getText().charAt(2)).toUpperCase()+" "+one.get(0).getKills()+"\n");break;
			}
			fw.close();
		}catch (IOException e){}
	}
	public void repaintStuff()
	{
		this.repaint();
	}
	public void setCheatMode(boolean cheatMode)
	{
		this.cheatMode=cheatMode;
	}
	
}
