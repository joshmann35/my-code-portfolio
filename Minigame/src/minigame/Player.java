package minigame;

import java.util.ArrayList;
import java.util.List;

import ref.GAME_REF;

public class Player extends Unit{
	private int kills;
	private int shots;
	private int numOfPows;
	private List<Powerup> p;
	Player(int x, int y, int width, int height, int health, int borderX, int borderY) {
		super(x, y, width, height, health, borderX, borderY);
		kills = GAME_REF.START_KILLS;
		shots = GAME_REF.START_SHOTS;
		numOfPows = GAME_REF.START_NUM_OF_POWS;
		p = new ArrayList<Powerup>();
	}
	
	public void addPowerup(Powerup ps)
	{
		p.add(ps);
		numOfPows++;
	}
	
	public void usePowerup()
	{
		numOfPows--;
		p.remove(0);
	}
	
	public int getNumOfPows()
	{
		return numOfPows;
	}
	
	public void setNumOfPows(int num)
	{
		this.numOfPows=num;
	}
	
	public void addEnemyKilled()
	{
		kills++;
	}
	public int getKills()
	{
		return kills;
	}
	public int getShots()
	{
		return shots;
	}
	public void incShots()
	{
		shots++;
	}
	public void decShots()
	{
		shots--;
	}
	public void setShots(int shots)
	{
		this.shots=shots;
	}
	public List<Powerup> getPList()
	{
		return p;
	}

}
