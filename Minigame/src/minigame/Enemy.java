package minigame;

import java.util.List;

public class Enemy extends Unit{
	Enemy(int x, int y, int width, int height, int health, int borderX, int borderY) {
		super(x, y, width, height, health, borderX, borderY);
	}
	public void move(List <Player> one, double randSpeed)
	{
		
		int xSpeed = ((int)(one.get(0).getX()-(this.getX()+this.getW()/2)));
		int ySpeed = ((int)(one.get(0).getY()-(this.getY()+this.getH()/2)));
		double direction = Math.atan2(ySpeed, xSpeed);
		double speed = Math.sqrt(xSpeed*xSpeed + ySpeed*ySpeed);
	    speed = Math.min(speed, randSpeed);
	    int x_inc = (int)(speed * Math.cos(direction));
	    int y_inc = (int)(speed * Math.sin(direction));
	    this.setX(this.getX()+x_inc);
		this.setY(this.getY()+y_inc);
		super.checkOverShoot();
	}

}
