
package minigame;

import java.awt.Rectangle;

public class Powerup
{
	private int x, y, w, h, type;
	Rectangle r;
	public Powerup(int x, int y, int w, int h, int type)
	{
		this.x=x;
		this.y=y;
		this.w=w;
		this.h=h;
		this.type=type;
		r = new Rectangle(x,y,w,h);
		
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getW() {
		return w;
	}
	public void setW(int w) {
		this.w = w;
	}
	public int getH() {
		return h;
	}
	public void setH(int h) {
		this.h = h;
	}
	public Rectangle getRect()
	{
		return this.r.getBounds();
	}
	public int getType()
	{
		return this.type;
	}
	public String getTypeString()
	{
		switch(type)
		{
		case 0: return "Freeze Shield"; 
		case 1: return "Ion Shot"; 
		case 2: return "Multi-Shot"; 
		default: return "No Type";
		}
	}
	public void setType(int type)
	{
		this.type=type;
	}
}
