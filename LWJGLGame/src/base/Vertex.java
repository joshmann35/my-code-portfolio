
package base;

public class Vertex
{
	private VertexPoint x, y, z;
	public Vertex(float x, float y, float z)
	{
		this.x = new VertexPoint(x);
		this.y = new VertexPoint(y);
		this.z = new VertexPoint(z);
	}
	public VertexPoint getX()
	{
		return x;
	}
	public VertexPoint getY()
	{
		return y;
	}
	public VertexPoint getZ()
	{
		return z;
	}
}
