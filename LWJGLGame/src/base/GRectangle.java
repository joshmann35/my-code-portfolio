
package base;

public class GRectangle
{
	private Vertex a, b, c, d;
	private float x, y, z;
	private double step;
	
	public GRectangle(Vertex a, Vertex b, Vertex c, Vertex d)
	{
		x = 0;
		y = 0;
		z = 0;
		step = 0.1;
		this.a=a;
		this.b=b;
		this.c=c;
		this.d=d;
	}
	public void setVertex1(float x, float y, float z)
	{
		a = new Vertex(x,y,z);
	}
	public void setVertex2(float x, float y, float z)
	{
		b = new Vertex(x,y,z);
	}
	public void setVertex3(float x, float y, float z)
	{
		c = new Vertex(x,y,z);
	}
	public void setVertex4(float x, float y, float z)
	{
		d = new Vertex(x,y,z);
	}
	public Vertex getVertex1()
	{
		return a;
	}
	public Vertex getVertex2()
	{
		return b;
	}
	public Vertex getVertex3()
	{
		return c;
	}
	public Vertex getVertex4()
	{
		return d;
	}
	public void incX()
	{
		x+=step;
	}
	public void incY()
	{
		y+=step;
	}
	public void incZ()
	{
		z+=step;
	}
	public void decX()
	{
		x-=step;
	}
	public void decY()
	{
		y-=step;
	}
	public void decZ()
	{
		z-=step;
	}
	public float getX()
	{
		return x;
	}
	public float getY()
	{
		return y;
	}
	public float getZ()
	{
		return z;
	}
	public void setX(float x)
	{
		this.x = x;
	}
	public void setY(float y)
	{
		this.y = y;
	}
	public void setZ(float z)
	{
		this.z = z;
	}
	
}
