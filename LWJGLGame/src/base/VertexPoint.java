
package base;

public class VertexPoint
{
	private float a;
	public VertexPoint(float a)
	{
		this.a=a;
	}
	public float getPoint()
	{
		return a;
	}
	public void setA(float a)
	{
		this.a=a;
	}
}
