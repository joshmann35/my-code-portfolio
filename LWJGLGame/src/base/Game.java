
package base;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT_SHIFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_SPACE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetFramebufferSize;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL11.glVertex3f;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL11.glScaled;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;

public class Game
{
	public static GRectangle player;
	private static float size = 1;
	public static boolean[] keys = {false, false, false, false, false, false};
	
	 private static GLFWErrorCallback errorCallback = GLFWErrorCallback.createPrint(System.err);

	 /**
	  * This key callback will check if ESC is pressed and will close the window
	  *if it is pressed.
	  */
	 private static GLFWKeyCallback keyCallback = new GLFWKeyCallback() {
		 
		 @Override
		 public void invoke(long window, int key, int scancode, int action, int mods) {
			 if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
				 glfwSetWindowShouldClose(window, GLFW_TRUE);
			 }
			 if(key == GLFW_KEY_W && action == GLFW_PRESS)
			 {
				 keys[0]=true;
			 }
			 if(key == GLFW_KEY_A && action == GLFW_PRESS)
			 {
				 keys[1]=true;
			 }
			 if(key == GLFW_KEY_S && action == GLFW_PRESS)
			 {
				 keys[2]=true;
			 }
			 if(key == GLFW_KEY_D && action == GLFW_PRESS)
			 {
				 keys[3]=true;
			 }
			 if( key == GLFW_KEY_LEFT_SHIFT && action == GLFW_PRESS)
			 {
				 keys[4]=true;
			 }
			 if(key == GLFW_KEY_SPACE && action == GLFW_PRESS)
			 {
				 keys[5]=true;
			 }
			 if(key == GLFW_KEY_W && action == GLFW_RELEASE)
			 {
				 keys[0]=false;
			 }
			 if(key == GLFW_KEY_A && action == GLFW_RELEASE)
			 {
				 keys[1]=false;
			 }
			 if(key == GLFW_KEY_S && action == GLFW_RELEASE)
			 {
				 keys[2]=false;
			 }
			 if(key == GLFW_KEY_D && action == GLFW_RELEASE)
			 {
				 keys[3]=false;
			 }
			 if( key == GLFW_KEY_LEFT_SHIFT && action == GLFW_RELEASE)
			 {
				 keys[4]=false;
			 }
			 if(key == GLFW_KEY_SPACE && action == GLFW_RELEASE)
			 {
				 keys[5]=false;
			 }
		}
	};
	
	
	public static void main(String[] args)
	{
		
		player = new GRectangle(new Vertex(size, size, 0), new Vertex(-size, size, 0), new Vertex(-size, -size, 0), new Vertex(size, -size, 0));
		long window;
		glfwSetErrorCallback(errorCallback);

        /* Initialize GLFW */
        if (glfwInit() != GLFW_TRUE) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }

        /* Create window */
        window = glfwCreateWindow(640, 480, "Simple example", NULL, NULL);
        if (window == NULL) {
            glfwTerminate();
            throw new RuntimeException("Failed to create the GLFW window");
        }

        /* Center the window on screen */
        GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(window,
                (vidMode.width() - 640) / 2,
                (vidMode.height() - 480) / 2
        );
        /* Create OpenGL context */
        glfwMakeContextCurrent(window);
        GL.createCapabilities();
        
        /* Enable vertical synchronization */
        glfwSwapInterval(1);

        /* Set the key callback */
        glfwSetKeyCallback(window, keyCallback);

        /* Declare buffers for using inside the loop */
        IntBuffer width = BufferUtils.createIntBuffer(1);
        IntBuffer height = BufferUtils.createIntBuffer(1);
        float rtri = 0;
        float rquad = 0;
        while (glfwWindowShouldClose(window) != GLFW_TRUE) {
        	
            float ratio;
            for(int i = 0; i < keys.length; i++)
            {
            	if(keys[i])
            	{
            		switch(i)
            		{
            		case 0: player.incY(); break;
            		case 1: player.decX();break;
            		case 2: player.decY();break;
            		case 3: player.incX();break;
            		case 4: player.decZ();break;
            		case 5: player.incZ();break;
            		}
            	}
            }
            

            /* Get width and height to calcualte the ratio */
            glfwGetFramebufferSize(window, width, height);
            ratio = width.get() / (float) height.get();

            /* Rewind buffers for next get */
            width.rewind();
            height.rewind();

            /* Set viewport and clear screen */
            glViewport(0, 0, width.get(), height.get());
            glClear(GL_COLOR_BUFFER_BIT);

            /* Set ortographic projection */
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(-ratio*10, ratio*10, -10f, 10f, 100f, -100f);
            
            glMatrixMode(GL_MODELVIEW);

            /* Rotate matrix */
            glLoadIdentity();
            //glRotatef((float) glfwGetTime() * 80f, 80f, 200f, f);
            
            /* Render triangle */
            
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
            glLoadIdentity();                   // Reset The View
            glTranslatef(player.getX(),player.getY(),player.getZ());             // Move Left And Into The Screen
            
            //glRotatef(rtri,0.0f,1.0f,0.0f);             // Rotate The Pyramid On It's Y Axis
            glBegin(GL_QUADS);
            glColor3f(1.0f, 0.0f, 0.0f);
//            glScalef(player.getVertex1().getX().getPoint(), player.getVertex1().getY().getPoint(), player.getVertex1().getZ().getPoint());
//            glScalef(player.getVertex2().getX().getPoint(), player.getVertex2().getY().getPoint(), player.getVertex2().getZ().getPoint());
//            glScalef(player.getVertex3().getX().getPoint(), player.getVertex3().getY().getPoint(), player.getVertex3().getZ().getPoint());
//            glScalef(player.getVertex4().getX().getPoint(), player.getVertex4().getY().getPoint(), player.getVertex4().getZ().getPoint());
//            glScaled(player.getVertex4().getX().getPoint(), player.getVertex4().getY().getPoint(), player.getVertex4().getZ().getPoint());
//            glScaled(player.getVertex1().getX().getPoint(), player.getVertex1().getY().getPoint(), player.getVertex1().getZ().getPoint());
//            glScaled(player.getVertex2().getX().getPoint(), player.getVertex2().getY().getPoint(), player.getVertex2().getZ().getPoint());
//            glScaled(player.getVertex3().getX().getPoint(), player.getVertex3().getY().getPoint(), player.getVertex3().getZ().getPoint());
            glVertex3f(player.getVertex1().getX().getPoint(), player.getVertex1().getY().getPoint(), player.getVertex1().getZ().getPoint());
            glVertex3f(player.getVertex2().getX().getPoint(), player.getVertex2().getY().getPoint(), player.getVertex2().getZ().getPoint());
            glVertex3f(player.getVertex3().getX().getPoint(), player.getVertex3().getY().getPoint(), player.getVertex3().getZ().getPoint());
            glVertex3f(player.getVertex4().getX().getPoint(), player.getVertex4().getY().getPoint(), player.getVertex4().getZ().getPoint());
            glEnd();
         
//            glBegin(GL_TRIANGLES);
//            glColor3f(1.0f,0.0f,0.0f);          // Red
//            glVertex3f( 0.0f, 1.0f, 0.0f);          // Top Of Triangle (Front)
//            glColor3f(0.0f,1.0f,0.0f);          // Green
//            glVertex3f(-1.0f,-1.0f, 1.0f);          // Left Of Triangle (Front)
//            glColor3f(1f,1.0f,1.0f);          // Blue
//            glVertex3f( 1.0f,-1.0f, 1.0f);
//            glColor3f(1.0f,0.0f,0.0f);          // Red
//            glVertex3f( 0.0f, 1.0f, 0.0f);          // Top Of Triangle (Right)
//            glColor3f(0.0f,0.0f,1.0f);          // Blue
//            glVertex3f( 1.0f,-1.0f, 1.0f);          // Left Of Triangle (Right)
//            glColor3f(0.0f,1.0f,0.0f);          // Green
//            glVertex3f( 1.0f,-1.0f, -1.0f);
//            glColor3f(1.0f,0.0f,0.0f);          // Red
//            glVertex3f( 0.0f, 1.0f, 0.0f);          // Top Of Triangle (Back)
//            glColor3f(0.0f,1.0f,0.0f);          // Green
//            glVertex3f( 1.0f,-1.0f, -1.0f);         // Left Of Triangle (Back)
//            glColor3f(0.0f,0.0f,1.0f);          // Blue
//            glVertex3f(-1.0f,-1.0f, -1.0f);
//            glColor3f(1.0f,0.0f,0.0f);          // Red
//            glVertex3f( 0.0f, 1.0f, 0.0f);          // Top Of Triangle (Left)
//            glColor3f(0.0f,0.0f,1.0f);          // Blue
//            glVertex3f(-1.0f,-1.0f,-1.0f);          // Left Of Triangle (Left)
//            glColor3f(0.0f,1.0f,0.0f);          // Green
//            glVertex3f(-1.0f,-1.0f, 1.0f);          // Right Of Triangle (Left)
//            glEnd();
//            glLoadIdentity();
//            glTranslatef(1.5f,0.0f,-7.0f);              // Move Right And Into The Screen
//             
//            glRotatef(rquad,1.0f,1.0f,1.0f);            // Rotate The Cube On X, Y & Z
//             
//            glBegin(GL_QUADS);
//            glColor3f(0.0f,1.0f,0.0f);          // Set The Color To Green
//            glVertex4f( 1.0f, 1.0f,-1.0f, 1f);          // Top Right Of The Quad (Top)
//            glVertex4f(-1.0f, 1.0f,-1.0f, 1f);          // Top Left Of The Quad (Top)
//            glVertex4f(-1.0f, 1.0f, 1.0f, 1f);          // Bottom Left Of The Quad (Top)
//            glVertex4f( 1.0f, 1.0f, 1.0f, 1f);
//            glColor3f(1.0f,0.5f,0.0f);          // Set The Color To Orange
//            glVertex4f( 1.0f,-1.0f, 1.0f, 1f);          // Top Right Of The Quad (Bottom)
//            glVertex4f(-1.0f,-1.0f, 1.0f, 1f);          // Top Left Of The Quad (Bottom)
//            glVertex4f(-1.0f,-1.0f,-1.0f, 1f);          // Bottom Left Of The Quad (Bottom)
//            glVertex4f( 1.0f,-1.0f,-1.0f, 1f);
//            glColor3f(1.0f,0.0f,0.0f);          // Set The Color To Red
//            glVertex4f( 1.0f, 1.0f, 1.0f, 1f);          // Top Right Of The Quad (Front)
//            glVertex4f(-1.0f, 1.0f, 1.0f, 1f);          // Top Left Of The Quad (Front)
//            glVertex4f(-1.0f,-1.0f, 1.0f, 1f);          // Bottom Left Of The Quad (Front)
//            glVertex4f( 1.0f,-1.0f, 1.0f, 1f);
//            glColor3f(1.0f,1.0f,0.0f);          // Set The Color To Yellow
//            glVertex4f( 1.0f,-1.0f,-1.0f, 1f);          // Bottom Left Of The Quad (Back)
//            glVertex4f(-1.0f,-1.0f,-1.0f, 1f);          // Bottom Right Of The Quad (Back)
//            glVertex4f(-1.0f, 1.0f,-1.0f, 1f);          // Top Right Of The Quad (Back)
//            glVertex4f( 1.0f, 1.0f,-1.0f, 1f);
//            glColor3f(0.0f,0.0f,1.0f);          // Set The Color To Blue
//            glVertex4f(-1.0f, 1.0f, 1.0f, 1f);          // Top Right Of The Quad (Left)
//            glVertex4f(-1.0f, 1.0f,-1.0f, 1f);          // Top Left Of The Quad (Left)
//            glVertex4f(-1.0f,-1.0f,-1.0f, 1f);          // Bottom Left Of The Quad (Left)
//            glVertex4f(-1.0f,-1.0f, 1.0f, 1f);
//            glColor3f(1.0f,0.0f,1.0f);          // Set The Color To Violet
//            glVertex4f( 1.0f, 1.0f,-1.0f, 1f);          // Top Right Of The Quad (Right)
//            glVertex4f( 1.0f, 1.0f, 1.0f, 1f);          // Top Left Of The Quad (Right)
//            glVertex4f( 1.0f,-1.0f, 1.0f, 1f);          // Bottom Left Of The Quad (Right)
//            glVertex4f( 1.0f,-1.0f,-1.0f, 1f);          // Bottom Right Of The Quad (Right)
//            glEnd();                        // Done Drawing The Quad
     
            rtri+=5f;                     // Increase The Rotation Variable For The Triangle
            rquad-=5f;
            /* Swap buffers and poll Events */
            glfwSwapBuffers(window);
            glfwPollEvents();

            /* Flip buffers for next loop */
            width.flip();
            height.flip();
            glfwPollEvents();
            
        }

        /* Release window and its callbacks */
        glfwDestroyWindow(window);
        keyCallback.release();

        /* Terminate GLFW and release the error callback */
        glfwTerminate();
        errorCallback.release();
	}
	

}
